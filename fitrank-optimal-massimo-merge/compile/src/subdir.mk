################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
    ../src/addi-fit2_ottimizzato.cpp \
    ../src/balance.cpp \
    ../src/loadobj.cpp \
    ../src/mb_main.cpp \
    ../src/render.cpp 

OBJS += \
    ./src/addi-fit2_ottimizzato.o \
    ./src/balance.o \
    ./src/loadobj.o \
    ./src/mb_main.o \
    ./src/render.o

CPP_DEPS += \
    ./src/addi-fit2_ottimizzato.d \
    ./src/balance.d \
    ./src/loadobj.d \
    ./src/mb_main.d \
    ./src/render.d

# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++-7 -std=c++14 -I/usr/local/include -I/usr/local/include/opencv -O3 -march=native -Wall -c -fmessage-length=0 -pthread -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


