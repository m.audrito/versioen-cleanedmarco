
#include "utility.h"
#include <iostream>
#include <random>

using namespace std;
using namespace cv;

#define RANDOM_SHUFFLE      my_random_shuffle
//--------------------------------------------------------------------------------------------------
template< class RandomIt >
void my_random_shuffle(RandomIt first, RandomIt last)
{
	typename std::iterator_traits<RandomIt>::difference_type i, n;
	n = last - first;
	for (i = n - 1; i > 0; --i) {
		using std::swap;
		swap(first[i], first[std::rand() % (i + 1)]);
	}
}

Point3f midpoint(const std::vector<Point3f>& vertices)
{
    Point3f medio = {0.0f, 0.0f, 0.0f};
    for (auto& vertice : vertices)
    {
        medio += vertice;
    }
    medio.x /= float(vertices.size());
    medio.y /= float(vertices.size());
    medio.z /= float(vertices.size());
    return medio;
}

static Line_s fit_line(const std::vector<Point>& pp)
{
    double sum_x = 0.0;
    double sum_y = 0.0;
    double sum_xy = 0.0;
    double sum_x2 = 0.0;


    for (auto& p : pp)
    {
        sum_x += p.x;
        sum_y += p.y;
        sum_xy += p.x * p.y;
        sum_x2 += p.x * p.x;
    }

    const double mean_x = sum_x / pp.size();
    const double mean_y = sum_y / pp.size();
    const double varx = sum_x2 - sum_x * mean_x;
    const double cov = sum_xy - sum_x * mean_y;

    if (abs(varx) > 100.0 * std::numeric_limits<double>::epsilon())
    {
        const double m = cov / varx;
        const double q = mean_y - m * mean_x;
        return {m, q, 1};
    }
    else
    {
        const double m = 0.0;
        const double q = mean_x;
        return {m, q, 2};
    }
}

static double distance_point_to_line(Point p, Point2d p0, Point2d r)
{
    const Point2d s(p.x - p0.x, p.y - p0.y);
    return abs(s.x * r.y - s.y * r.x);
}
//
//Line_s fit_line_w_ransac(std::vector<Point>& pp, int iterations, double maxError, int inliers_min_percent, bool first_half)
//{
//    double min_error = std::numeric_limits<double>::max();
//    Line_s best_line = {0, 0, 0};
//
//    if (pp.size() < 2) return best_line;
//
//    std::random_device rd;
//    std::mt19937 g(rd());
//
//    for (int i = 0; i < iterations; ++i)
//    {
//        if (first_half)
//        {
//            shuffle(pp.begin(), pp.begin() + pp.size() / 2, g);
//        }
//        else
//        {
//			//cout << pp << endl;
//            shuffle(pp.begin(), pp.end(), g);
//        }
//		//cout << "shuffled   "  << pp << endl;
//        std::vector<Point> inliers(pp.begin(), pp.begin() + 2);
//        std::vector<Point> outliers(pp.begin() + 2, pp.end());
//        auto line = fit_line(inliers);
//
//        if (line.type == 0) continue;
//        auto& m = line.m;
//        auto& q = line.q;
//
//        auto f = sqrt(1.0 + m * m);
//        Point2d p0;
//        Point2d r;
//        if (line.type == 1)
//        {
//            r = Point2d(1.0 / f, m / f);
//            p0 = Point2d(0.0, q);
//        }
//        else
//        {
//            r = Point2d(m / f, 1.0 / f);
//            p0 = Point2d(q, 0.0);
//        }
//        for (auto& p : outliers)
//            if (distance_point_to_line(p, p0, r) < maxError)
//                inliers.push_back(p);
//
//        const size_t min_inliers = pp.size() * inliers_min_percent / 100;
//        if (inliers.size() < min_inliers) continue;
//
//        line = fit_line(inliers);
//        if (line.type == 0) continue;
//        m = line.m;
//        q = line.q;
//        f = sqrt(1 + m * m);
//        if (line.type == 1)
//        {
//            r = Point2d(1.0 / f, m / f);
//            p0 = Point2d(0.0, q);
//        }
//        else
//        {
//            r = Point2d(m / f, 1.0 / f);
//            p0 = Point2d(q, 0.0);
//        }
//        double error = 0.0;
//        for (auto& p : inliers)
//            error += distance_point_to_line(p, p0, r);
//
//        if (error < min_error)
//        {
//            min_error = error;
//            best_line = line;
//        }
//    }
//
//    return best_line;
//}

//--------------------------------------------------------------------------------------------------
Line_s fit_line_w_ransac(vector<Point> &pp, int iterations, double maxError, int inliers_min_percent, bool first_half ) {

	double min_error = numeric_limits<double>::max();
	Line_s best_line = { 0,0,0 };

	if (pp.size() < 2) return best_line;

	for (int i = 0; i < iterations; ++i) {
		if (first_half) RANDOM_SHUFFLE(&pp[0], &pp[pp.size() / 2]);
		else RANDOM_SHUFFLE(pp.begin(), pp.end());
		vector<Point> inliers(pp.begin(), pp.begin() + 2);
		vector<Point> outliers(pp.begin() + 2, pp.end());
		auto line = fit_line(inliers);

		if (line.type == 0) continue;
		auto &m = line.m;
		auto &q = line.q;

		auto f = sqrt(1.0 + m * m);
		Point2d p0;
		Point2d r;
		if (line.type == 1) {
			r = Point2d(1.0 / f, m / f);
			p0 = Point2d(0.0, q);
		}
		else {
			r = Point2d(m / f, 1.0 / f);
			p0 = Point2d(q, 0.0);
		}
		for (auto &p : outliers)
			if (distance_point_to_line(p, p0, r) < maxError)
				inliers.push_back(p);

		size_t min_inliers = (size_t)(pp.size() * (inliers_min_percent / 100.0));
		if (inliers.size() < min_inliers) continue;

		line = fit_line(inliers);
		if (line.type == 0) continue;
		m = line.m;
		q = line.q;
		f = sqrt(1 + m * m);
		if (line.type == 1) {
			r = Point2d(1.0 / f, m / f);
			p0 = Point2d(0.0, q);
		}
		else {
			r = Point2d(m / f, 1.0 / f);
			p0 = Point2d(q, 0.0);
		}
		double error = 0.0;
		for (auto &p : inliers)
			error += distance_point_to_line(p, p0, r);

		if (error < min_error) {
			min_error = error;
			best_line = line;
		}
	}

	return best_line;
}



Point2d compute_corner(Line_s& line1, Line_s& line2)
{
    auto& t1 = line1.type;
    auto& t2 = line2.type;

    auto& m1 = line1.m;
    auto& q1 = line1.q;
    auto& m2 = line2.m;
    auto& q2 = line2.q;

    if (t1 == 1 && t2 == 1)
    {
        const double n = q2 - q1;
        const double d = m1 - m2;
        if (abs(d) > std::numeric_limits<double>::epsilon())
        {
            const double x = n / d;
            const double y = m1 * x + q1;
            return Point2d(x, y);
        }
    }
    else if (t1 == 1 && t2 == 2)
    {
        const double x = q2;
        const double y = m1 * x + q1;
        return Point2d(x, y);
    }
    else if (t1 == 2 && t2 == 1)
    {
        const double x = q1;
        const double y = m2 * x + q2;
        return Point2d(x, y);
    }
    return Point2d(0, 0);
}

bool angle_between_lines_is_bad(const Line_s& l1, const Line_s& l2)
{
    if (l1.type == 1 && l2.type == 1)
    {
        const double m1 = l1.m;
        const double m2 = l2.m;
        return abs((m2 - m1) / (1 + m1 * m2)) < 1.0;
    }
    if (l1.type == 2 && l2.type == 1)
    {
        return l2.m > 1.0;
    }
    if (l1.type == 1 && l2.type == 2)
    {
        return l1.m > 1.0;
    }
    return true;
}

std::vector<double> convolve(const std::vector<double>& a, const std::vector<double>& b)
{
    std::vector<double> r(a.size());
    const int h = static_cast<int>(b.size()) / 2;

    for (int i = 0; i < a.size(); ++i)
    {
        for (int k = -h; k <= h; ++k)
        {
            if ((i + k) >= a.size() || (i + k) < 0) continue;
            r[i] += a[i + k] * b[h - k];
        }
    }

    return r;
}
