#pragma once

#ifndef __BALANCE_H__
#define __BALANCE_H__

#include <opencv2/core/mat.hpp>

/**
 * Auto Levels the image histogram, by setting 90% of the samples to be fill the range 0-255 (per channel)
 * @param mat Ingresso/uscita immagine da bilanciare.
 */
void autoLevels(cv::Mat &mat);

/**
 * Applica un filtro sigmoide all�immagine per enfatizzare il contrasto.
 * @param src Immagine di partenza sorgente.
 * @param dst Variabile per la memorizzazione dell�immagine elaborata.
 * @param E Pendenza del filtro.
 * @param m Centratura sigmoide.
 */
void sigmoide_contrasto(const cv::Mat& src, cv::Mat& dst, float E, float m);

cv::Mat color_balance(const cv::Mat& im);
void do_threshold(const cv::Mat& im, cv::Mat& bw);
cv::Mat do_threshold(const cv::Mat& im);

#endif
