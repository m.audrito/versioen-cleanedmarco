#include <vector>
#include <limits>
#include <iostream>
#include <future>

#include <opencv2/opencv.hpp>

#include "mb_main.h"
#include "debugging.h"
#include "balance.h"

#define MB_ERROR_OK                     0
#define MB_ERROR_BAD_IMAGE              1
#define MB_ERROR_IMAGE_SZ_DISAGREE      2
#define MB_ERROR_NO_A4                  3
#define MB_ERROR_NO_FOOT                4
#define MB_ERROR_NO_HOMOGRAPHY          5
#define MB_ERROR_INDECISION             6

constexpr double pi = 3.141592653589793238462643383279502884;
constexpr double _180overpi = 180.0 / pi;
constexpr double _360overpi = 360.0 / pi;
//--------------------------------------------------------------------------------------------------
using namespace std;
using namespace cv;

//--------------------------------------------------------------------------------------------------
struct Corner_s {
    Line_s line1;
    Line_s line2;
    Point2d corner;
};


//--------------------------------------------------------------------------------------------------
struct Heel_s {
    Rect roi;
    Mat im;
    Mat mask;
};


//--------------------------------------------------------------------------------------------------
struct Areas_s {
    Mask_s on_sheet;
    Mask_s on_floor;
    std::vector<Point> points_sheet;
    std::vector<Point> points_floor;
};


//--------------------------------------------------------------------------------------------------
struct Process_A4_Args_s {
    Mat &im;
    const AlgoParams *params;
    Process_A4_s a4;
};


//--------------------------------------------------------------------------------------------------
struct Process_Foot_Args_s {
    const Process_A4_s &a4;
    const AlgoParams *params;
    Point toe;
    std::vector<Point> heel_profile;
    std::vector<Point> heel_c_profile;
    std::vector<Point> heel_pts;
};


//--------------------------------------------------------------------------------------------------
static int imload_and_resize(const char *fname, Mat &im, const AlgoParams *params) {

    im = imread(fname, IMREAD_COLOR);
    if (im.cols == 0 || im.rows == 0) return MB_ERROR_BAD_IMAGE;
    if (im.cols > im.rows) {
        flip(im, im, 1);
        transpose(im, im);
    }


    /* Sarebbe bene fare il resize dell'immagine verso una dimensione fissa, ad es. QXGA
     */


    if (params->debug >= 3) imsave(im, "Original");
    return 0;
}

//--------------------------------------------------------------------------------------------------
static Mask_s extract_A4(Mat &im, const AlgoParams *params) {
    Mat th = do_threshold(im);

    Mat tmp;
    erode(th, tmp, Mat::ones(3,3,CV_8U));
    dilate(tmp, th, Mat::ones(3,3,CV_8U));

    Mat cnn;
    Mat stats;
    Mat centroids;
    const int n = connectedComponentsWithStats(th, cnn, stats, centroids, 4);

    const int32_t Area = im.cols*im.rows;
    const int32_t minArea = Area * params->a4_minAreaPerCent / 100;

    Rect bb;
    int32_t max_area = 0;
    int max_i = 0;
    for (int i=1; i<n; ++i) {
        const int area = stats.at<int32_t>(i, CC_STAT_AREA);

        if (area>minArea && area>max_area) {
            bb.x = stats.at<int32_t>(i, CC_STAT_LEFT);
            bb.y = stats.at<int32_t>(i, CC_STAT_TOP);
            bb.width = stats.at<int32_t>(i, CC_STAT_WIDTH);
            bb.height = stats.at<int32_t>(i, CC_STAT_HEIGHT);
            max_area = area;
            max_i = i;
        }
    }

    for (int y=0; y<th.rows; ++y) {
        const auto cptr = reinterpret_cast<int32_t*>(cnn.data + y * cnn.step);
        const auto tptr = th.data + y * th.step;
        for (int x=0; x<th.cols; ++x) {
            if (cptr[x] != max_i) {
                tptr[x] = 0;
            }
        }
    }

    if (params->debug >= 3) {
        Mat cth;
        cvtColor(th, cth, COLOR_GRAY2RGB);
        rectangle(cth, bb, Vec3b(0,0,255), 1, LINE_AA);
        imsave(cth, "Threshold for A4");
        imsave(th, "A4");
    }

    return {th, bb};
}

//--------------------------------------------------------------------------------------------------
static void check_range(const Mat &im, int * const x0, int * const x1, int * const y0, int * const y1) {
    const int x = 0;
    const int X = im.cols - 1;
    const int y = 0;
    const int Y = im.rows - 1;

    if (*x0 < x) *x0 = x;
    if (*x1 > X) *x1 = X;
    if (*y0 < y) *y0 = y;
    if (*y1 > Y) *y1 = Y;
}

//--------------------------------------------------------------------------------------------------
static void check_range(const Mat &im, Rect &r) {
	const int x = 0;
	const int X = im.cols - 1;
	const int y = 0;
	const int Y = im.rows - 1;

	int &x0 = r.x;
	int x1 = r.x + r.width;
	int &y0 = r.y;
	int y1 = r.y + r.height;

	if (x0 < x) x0 = x;
	if (x1 > X) x1 = X;
	if (y0 < y) y0 = y;
	if (y1 > Y) y1 = Y;

	r.width = x1 - x0;
	r.height = y1 - y0;
}

//--------------------------------------------------------------------------------------------------
enum {
    C_TOP_LEFT,
    C_TOP_RIGHT,
    C_BOTTOM_LEFT,
    C_BOTTOM_RIGHT,
};

static Corner_s find_corner(const Mat &im, const Rect &bb, const AlgoParams *params, int corner) {

	int x0 = bb.x;
	int x1 = x0 + bb.width - 1;
	int y0 = bb.y;
	int y1 = y0 + bb.height - 1;

	check_range(im, &x0, &x1, &y0, &y1);

	vector<Point> pp_hor;
	vector<Point> pp_ver;

    const int step = params->findCorners_skipPixels2;

	if (corner == C_TOP_LEFT) {
		for (int x = x1; x >= x0; x -= step) {
			for (int y = y0; y <= y1; ++y) {
                const uchar v = im.at<uchar>(y, x);
				if (v) {
					pp_hor.emplace_back(x, y);
					break;
				}
			}
		}
		for (int y = y1; y >= y0; y -= step) {
			for (int x = x0; x <= x1; ++x) {
                const uchar v = im.at<uchar>(y, x);
				if (v) {
					pp_ver.emplace_back(x, y);
					break;
				}
			}
		}
	}
	else if (corner == C_TOP_RIGHT) {
		for (int x = x0; x <= x1; x += step) {
			for (int y = y0; y <= y1; ++y) {
                const uchar v = im.at<uchar>(y, x);
				if (v) {
					pp_hor.emplace_back(x, y);
					break;
				}
			}
		}
		for (int y = y1; y >= y0; y -= step) {
			for (int x = x1; x >= x0; --x) {
                const uchar v = im.at<uchar>(y, x);
				if (v) {
					pp_ver.emplace_back(x, y);
					break;
				}
			}
		}
	}
	else if (corner == C_BOTTOM_LEFT) {
		for (int x = x1; x >= x0; x -= step) {
			for (int y = y1; y >= y0; --y) {
                const uchar v = im.at<uchar>(y, x);
				if (v) {
					pp_hor.emplace_back(x, y);
					break;
				}
			}
		}
		for (int y = y0; y <= y1; y += step) {
			for (int x = x0; x <= x1; ++x) {
                const uchar v = im.at<uchar>(y, x);
				if (v) {
					pp_ver.emplace_back(x, y);
					break;
				}
			}
		}
	}
	else if (corner == C_BOTTOM_RIGHT) {
		for (int x = x0; x <= x1; x += step) {
			for (int y = y1; y >= y0; --y) {
                const uchar v = im.at<uchar>(y, x);
				if (v) {
					pp_hor.emplace_back(x, y);
					break;
				}
			}
		}
		for (int y = y0; y <= y1; y += step) {
			for (int x = x1; x >= x0; --x) {
                const uchar v = im.at<uchar>(y, x);
				if (v) {
					pp_ver.emplace_back(x, y);
					break;
				}
			}
		}
	}
    
	auto line1 = fit_line_w_ransac(pp_hor, params->ransac2_iterations, params->ransac2_maxError, params->ransac2_minInliersPercent, true);
	if (line1.type == 0) line1 = fit_line_w_ransac(pp_hor, params->ransac2_iterations, params->ransac2_maxError, params->ransac2_minInliersPercent_2nd, true);
	auto line2 = fit_line_w_ransac(pp_ver, params->ransac2_iterations, params->ransac2_maxError, params->ransac2_minInliersPercent, true);
	if (line2.type == 0)  line2 = fit_line_w_ransac(pp_ver, params->ransac2_iterations, params->ransac2_maxError, params->ransac2_minInliersPercent_2nd, true);

	if (angle_between_lines_is_bad(line1, line2))
		return {};

	const Point2d c = compute_corner(line1, line2);

	return { line1, line2, c };
}

//--------------------------------------------------------------------------------------------------
std::vector<Point2f> find_a4_corners(const Mat &orim, Mask_s &a4, const AlgoParams *params) {

    auto &im = a4.mask;
    auto &bb = a4.bb;

    int x0 = bb.x;
    int x1 = x0 + bb.width;
    int y0 = bb.y;
    int y1 = y0 + bb.height;

    check_range(im, &x0, &x1, &y0, &y1);

    vector<Point> p_top;
    for (int x = x0; x < x1; x += params->findCorners_skipPixels) {
        for (int y = y0; y < y1; ++y) {
            const uchar v = im.at<uchar>(y, x);
            if (v) {
                p_top.emplace_back(x, y);
                break;
            }
        }
    }

    vector<Point> p_left;
    for (int y = y0; y < y1; y += params->findCorners_skipPixels) {
        for (int x = x0; x < x1; ++x) {
            const uchar v = im.at<uchar>(y, x);
            if (v) {
                p_left.emplace_back(x, y);
                break;
            }
        }
    }

    vector<Point> p_bottom;
    for (int x = x0; x < x1; x += params->findCorners_skipPixels) {
        for (int y = y1; y > y0; --y) {
            const uchar v = im.at<uchar>(y, x);
            if (v) {
                p_bottom.emplace_back(x, y);
                break;
            }
        }
    }

    vector<Point> p_right;
    for (int y = y0; y < y1; y += params->findCorners_skipPixels) {
        for (int x = x1; x > x0; --x) {
            const uchar v = im.at<uchar>(y, x);
            if (v) {
                p_right.emplace_back(x, y);
                break;
            }
        }
    }


    auto line_top = fit_line_w_ransac(p_top, params->ransac1_iterations, params->ransac1_maxError, params->ransac1_minInliersPercent);
    auto line_left = fit_line_w_ransac(p_left, params->ransac1_iterations, params->ransac1_maxError, params->ransac1_minInliersPercent);
    auto line_bottom = fit_line_w_ransac(p_bottom, params->ransac1_iterations, params->ransac1_maxError, params->ransac1_minInliersPercent);
    auto line_right = fit_line_w_ransac(p_right, params->ransac1_iterations, params->ransac1_maxError, params->ransac1_minInliersPercent);

    if (line_top.type == 0) line_top = fit_line_w_ransac(p_top, params->ransac1_iterations, params->ransac1_maxError, params->ransac1_minInliersPercent_2nd);
    if (line_left.type == 0) line_left = fit_line_w_ransac(p_left, params->ransac1_iterations, params->ransac1_maxError, params->ransac1_minInliersPercent_2nd);
    if (line_bottom.type == 0) line_bottom = fit_line_w_ransac(p_bottom, params->ransac1_iterations, params->ransac1_maxError, params->ransac1_minInliersPercent_2nd);
    if (line_right.type == 0) line_right = fit_line_w_ransac(p_right, params->ransac1_iterations, params->ransac1_maxError, params->ransac1_minInliersPercent_2nd);

    const auto c1 = compute_corner(line_top, line_left);
    const auto c2 = compute_corner(line_top, line_right);
    const auto c3 = compute_corner(line_bottom, line_left);
    const auto c4 = compute_corner(line_bottom, line_right);

    vector<Point2f> vertices;
    Rect r1, r2, r3, r4;
    Corner_s p1, p2, p3, p4;


    const auto is_null = [](const Point2d &p) {
        return p.x == 0.0 && p.y == 0.0;
    };
    if (!is_null(c1) && !is_null(c2) && !is_null(c3) && !is_null(c4)) {

        const int S = params->findCorners_roiSize;
        r1 = Rect(lround(c1.x - S + 0.5), lround(c1.y - S + 0.5), 2 * S, 2 * S);
        r2 = Rect(lround(c2.x - S + 0.5), lround(c2.y - S + 0.5), 2 * S, 2 * S);
        r3 = Rect(lround(c3.x - S + 0.5), lround(c3.y - S + 0.5), 2 * S, 2 * S);
        r4 = Rect(lround(c4.x - S + 0.5), lround(c4.y - S + 0.5), 2 * S, 2 * S);

        check_range(orim, r1);
        check_range(orim, r2);
        check_range(orim, r3);
        check_range(orim, r4);

        const Mat corner_mask(orim.size(), CV_8U, { 0,0,0 });
        const auto i1 = orim(r1);
        auto m1 = corner_mask(r1);
        do_threshold(i1, m1);
        const auto i2 = orim(r2);
        auto m2 = corner_mask(r2);
        do_threshold(i2, m2);
        const auto i3 = orim(r3);
        auto m3 = corner_mask(r3);
        do_threshold(i3, m3);
        const auto i4 = orim(r4);
        auto m4 = corner_mask(r4);
        do_threshold(i4, m4);

        p1 = find_corner(corner_mask, r1, params, C_TOP_LEFT);
        p2 = find_corner(corner_mask, r2, params, C_TOP_RIGHT);
        p3 = find_corner(corner_mask, r3, params, C_BOTTOM_LEFT);
        p4 = find_corner(corner_mask, r4, params, C_BOTTOM_RIGHT);

        vertices = { p1.corner, p2.corner, p3.corner, p4.corner };
        if (is_null(p1.corner) || is_null(p2.corner) || is_null(p3.corner) || is_null(p4.corner)) {
            vertices.clear();
        }
    }


    if (params->debug >= 3) {
        Mat cim;
        orim.copyTo(cim);

        draw_line(cim, line_top, 3000, {0, 0, 255});
        draw_line(cim, line_left, 3000, { 0, 0, 255 });
        draw_line(cim, line_bottom, 3000, { 0, 0, 255 });
        draw_line(cim, line_right, 3000, { 0, 0, 255 });

        rectangle(cim, r1, Vec3b(0, 0, 255), 2, LINE_AA);
        rectangle(cim, r2, Vec3b(0, 0, 255), 2, LINE_AA);
        rectangle(cim, r3, Vec3b(0, 0, 255), 2, LINE_AA);
        rectangle(cim, r4, Vec3b(0, 0, 255), 2, LINE_AA);

        imsave(cim, "A4 vertices (1)");

        orim.copyTo(cim);
        draw_line(cim, p1.line1, p1.corner, 3000, {0, 0, 255});
        draw_line(cim, p1.line2, p1.corner, 3000, {0, 0, 255});
        draw_line(cim, p2.line1, p2.corner, 3000, {0, 0, 255});
        draw_line(cim, p2.line2, p2.corner, 3000, {0, 0, 255});
        draw_line(cim, p3.line1, p3.corner, 3000, {0, 0, 255});
        draw_line(cim, p3.line2, p3.corner, 3000, {0, 0, 255});
        draw_line(cim, p4.line1, p4.corner, 3000, {0, 0, 255});
        draw_line(cim, p4.line2, p4.corner, 3000, {0, 0, 255});

        circle(cim, p1.corner, 3, Vec3b(255, 255, 0), 1, LINE_AA);
        circle(cim, p2.corner, 3, Vec3b(255, 255, 0), 1, LINE_AA);
        circle(cim, p3.corner, 3, Vec3b(255, 255, 0), 1, LINE_AA);
        circle(cim, p4.corner, 3, Vec3b(255, 255, 0), 1, LINE_AA);

        imsave(cim, "A4 vertices (2)");
    }

    return vertices;
}

//--------------------------------------------------------------------------------------------------
static Point2f my_normalize(const Point2f &p, double *len_ = nullptr) {
    const auto len = sqrt(p.x*p.x + p.y*p.y);
    if (len_) *len_ = len;
    return p / len;
}

//--------------------------------------------------------------------------------------------------
static void flip_image(vector<Point2f> &vv, Mask_s &a4, Mat &im, const AlgoParams *params) {

    auto p0 = vv[0];
    auto p1 = vv[1];
    auto p2 = vv[2];
    auto p3 = vv[3];

    auto &mask = a4.mask;

    const double D = 20.0;

    const auto r0 = my_normalize(p3 - p0);
    const auto r1 = my_normalize(p2 - p1);

    p0 += D*r0;
    p1 += D*r1;
    p2 -= D*r1;
    p3 -= D*r0;

    double L, R;
    const auto l = my_normalize(p2 - p0, &L);
    const auto r = my_normalize(p3 - p1, &R);

    int cl = 0;
    for (int t=0; t<L; ++t) {
        const Point2d p = p0 + t*l;
        const auto x = int(p.x);
        const auto y = int(p.y);
        if (mask.at<uchar>(y,x) == 0) ++cl;
    }

    int cr = 0;
    for (int t=0; t<R; ++t) {
        const Point2d p = p1 + t*r;
        const auto x = int(p.x);
        const auto y = int(p.y);
        if (mask.at<uchar>(y,x) == 0) ++cr;
    }


	if (cr > cl) {
		flip(im, im, -1);
		flip(mask, mask, -1);

		const float w = float(im.cols); // TODO: PAN: Check for an off-by-one error
		const float h = float(im.rows);
        for(auto &p: vv)
        {
            p.x = w - p.x;
            p.y = h - p.y;
        }

		swap(vv[0], vv[3]);
		swap(vv[1], vv[2]);
	}


	if (params->debug >= 3) {

		Mat cim;
		im.copyTo(cim);

        draw_vertices(cim, vv, { 0, 0, 255 }, 3, false);

	    imsave(cim, (cr > cl) ? "Rotated (yes)" : "Rotated (no)");
	}
}

//--------------------------------------------------------------------------------------------------
static Point toe_point(const Mat &, const Mask_s &a4, const vector<Point2f> &vv, const AlgoParams *) {

    auto &p0 = vv[0];
    auto &p1 = vv[1];
    auto &p2 = vv[2];
    auto &p3 = vv[3];


    // TODO: PAN: Check the meaning of all this stuff. Seems like it takes 40% IN PIXEL of both short sides, from p0 and p2 respectively (TWO OPPOSED VERTICES?!?) and then computes "L" as their distance. Then it loops from 30 (pixel?) to "L-30" pixel along this line, scanning 

    double l1;
    const auto v1 = my_normalize(p1-p0, &l1);
    double l2;
    const auto v2 = my_normalize(p3-p2, &l2);
    const auto m1 = p0 + v1*l1*0.4;
    const auto m2 = p2 + v2*l2*0.4;                    // *** magic number


    double L;
    const auto r = my_normalize(m2-m1, &L);


    auto &a4_mask = a4.mask;


    Point2f ep {-1,-1};
    for (int t=30; t<L-30; t+=2) {                     // *** magic number 
        const Point2f p = m1 + r*t;
        const auto y = int(p.y);
        int x = int(p.x);

        const uchar *ptr = a4_mask.data + a4_mask.step * y;

        Point ep1 = ep;
        while (ptr[x] == 0) {

            if (x > ep1.x) {
                ep1.x = x;
                ep1.y = y;
            }

            if (++x == a4_mask.cols) {
                break;
            }

            ep = ep1;
        }
    }

    return ep;
}

//--------------------------------------------------------------------------------------------------
static Point find_point_on_heel(const Mat &im, const Point p, const AlgoParams *params) {

    const int skip_x_r = params->heel_skip_x_right;
    const int skip_x_l = params->heel_skip_x_left;
    const int D = params->heel_derivative_step;
    const int M = params->heel_derivative_apices_window;
    const double N = params->heel_derivative_threshold;

    //int R = 40;         // intervallo di ricerca scostamenti fotometrici
    //double ff = 3.5;    // max scostamento fotometrico

    vector<double> r;
    vector<double> g;
    vector<double> b;
    vector<double> der;


    // Valori
    const int x0 = p.x - skip_x_r;
    const int y0 = p.y;
    const uchar *ptr = im.data + y0*im.step;
    for (int x=x0 ; x>skip_x_l; --x) {
        b.push_back(ptr[3 * x + 0]);
        g.push_back(ptr[3 * x + 1]);
        r.push_back(ptr[3 * x + 2]);
    }


    // Derivata dei valori
    for (size_t i=D; i<r.size()-D-1; ++i) {
        const auto dr = abs(r[i+D] - r[i-D]);
        const auto dg = abs(g[i+D] - g[i-D]);
        const auto db = abs(b[i+D] - b[i-D]);

        auto d = std::max(dr, std::max(dg, db));
        der.push_back(d);
    }

    // Miglior apice della derivata
    double mm = 0;
    int mi = -1;
    for (size_t i=M; i<der.size()-M-1; ++i) {
        auto m = 0.0;
        size_t k = 0;
        for (size_t j=i-M; j<=i+M; ++j) {
            if (der[j] > m) {
                m = der[j];
                k = j;
            }
        }
        m = der[i];
        if (k==i && m>N) {
            if (m > mm) {
                mm = m;
                mi = i;
            }
        }
    }

#if 1
    return mi>0 ? Point{x0-mi, y0} : Point{-1,-1};
#else
    // Scostamenti fotometrici
    auto stats = [R](const vector<double> &vals, int i) {
        double s=0.0;
        double s2=0.0;
        for (int j=i-R; j<=i; ++j) {
            auto v = vals[j];
            s += v;
            s2 += v*v;
        }
        auto m = s/R;
        return Vec2d(m, sqrt(s2/R-m*m));
    };

    auto cond = [&](int i) {
        auto s_r = stats(r, i);
        auto s_g = stats(g, i);
        auto s_b = stats(b, i);

        bool r__ = (r[i] > s_r[0] + ff*s_r[1]) || (r[i] < s_r[0] - ff*s_r[1]);
        bool g__ = (g[i] > s_g[0] + ff*s_g[1]) || (g[i] < s_g[0] - ff*s_g[1]);
        bool b__ = (b[i] > s_b[0] + ff*s_b[1]) || (b[i] < s_b[0] - ff*s_b[1]);

        return (r__ && b__ && g__) ? 1 : 0;
    };


    if (mi < 0) return {-1,-1};

    int c1 = cond(mi+1);
    int c2 = cond(mi+2);
    int c3 = cond(mi+3);
    int c4 = cond(mi+4);
    int c5 = cond(mi+5);
    int c =  c1 + c2 + c3 + c4 + c5;

    if (c >= 2) {
        return {x0-mi, y0};
    }
    else return {-1,-1};
#endif
}

//--------------------------------------------------------------------------------------------------
static vector<Point> pick_heel_points(const vector<Point> &pp, const AlgoParams *params) {

    const int T = params->heel_max_distance_between_points;

    vector<int> prox(pp.size(), -1);
    for (size_t i=0; i<pp.size(); ++i) {
        auto &p = pp[i];

        int m = 2*T*T;
        int mj = -1;
        for (size_t j=1 ; j<=5; ++j) {
            if (i+j >= pp.size()) break;

            auto &p1 = pp[i+j];

            const auto dx = p.x - p1.x;
            const auto dy = p.y - p1.y;
            const auto d = dx*dx + dy*dy;

            if (d < m) {
                m = d;
                mj = i+j;
            }
        }

        if (m < T*T) {
            prox[i] = mj;
        }
    }


    int bci = -1;
    size_t bcl = 0;
    for (size_t i=0; i<prox.size(); ++i) {
        size_t l = 1;
        int p = prox[i];
        while (p > 0) {
            ++l;
            p = prox[p];
        }
        if (l > bcl) {
            bcl = l;
            bci = i;
        }
    }

    vector<Point> ret;
    int p = bci;
    while (p >= 0) {
        ret.push_back(pp[p]);
        p = prox[p];
    }

    return ret;
}

//--------------------------------------------------------------------------------------------------
static vector<Point> heel_points(const Mat &im, const Mask_s &a4, const vector<Point2f> &vv, const AlgoParams *params) {

    auto &p0 = vv[0];
    auto &p1 = vv[1];
    auto &p2 = vv[2];
    auto &p3 = vv[3];


    double l1;
    const auto v1 = my_normalize(p1-p0, &l1);
    double l2;
	const auto v2 = my_normalize(p2 - p3, &l2);
	const auto m1 = p0 + v1 * 15;
	const auto m2 = p3 + v2 * 15;                                      // *** magic numbers
						

    double L;
    const auto r = my_normalize(m2-m1, &L);


    auto &a4_mask = a4.mask;


    double first = -1.0;
    double last = -1.0;
    const double max_len = 0.0; // PAN: Why is this never updated?
    for (int t=30; t<L-30; t+=2) {                     // *** magic number
        const Point2d p = m1 + r*t;
        const int y = int(p.y);
        const int x = int(p.x);
        const uchar *ptr = a4_mask.data + a4_mask.step * y;

        if (last<0.0 && first>0.0 && ptr[x]>0) {
            last = t;

            const auto len = last - first;
            if (len < max_len) {
                last = 0.0;
                first = 0.0;
            }
        }
        if (ptr[x]==0 && first<0.0) first = t;
    }


    const auto fp = m1 + first*r;
    const auto lp = m1 + last*r;
    const auto x0 = int((fp.x + lp.x) / 2.0);
    const auto y0 = int(fp.y);
    const auto y1 = int(lp.y);

    vector<Point> candidate_pts;
    for (int y=y0; y<=y1; y+=params->heel_y_search_step) {
        const auto pt = find_point_on_heel(im, {x0, y}, params);
        if (pt.x>0) candidate_pts.push_back(pt);
    }

    auto heel_pts = pick_heel_points(candidate_pts, params);
    return heel_pts;
}

//--------------------------------------------------------------------------------------------------
/// Routine di calibrazione della telecamera
/// uso di opencv come da documentazione e calcolo della matrice omografica
static int calibrate_camera_and_retrieve_poses(Process_Calibrate_Args_s &args) {

    vector<Vec2f> p1, p2, p3;
    for (const auto &p : args.pts1) p1.emplace_back(p.x, p.y);
    for (const auto &p : args.pts2) p2.emplace_back(p.x, p.y);
    for (const auto &p : args.pts3) p3.emplace_back(p.x, p.y);


    auto &K = args.K;
    auto &D = args.D;
    auto &rvecs = args.R;
    auto &tvecs = args.T;
    auto &H2 = args.H2;
    auto &H3 = args.H3;


    //-------- CALIBRATION ---------------
//	vector<Vec3f> obj = { {0.0f,0.0f,0.0f}, {210.0f,0.0f,0.0f}, {210.0f,-297.0f,0.0f}, {0.0f,-297.0f,0.0f} };
	const vector<Vec3f> obj = { {-148.5f,-105.0f,0.0f },{-148.5f, 105.0f,0.0f } ,{ 148.5f,105.0f,0.0f } ,{ 148.5f,-105.0f,0.0f } };

    const vector<vector<Vec3f>> obj_pts = {obj, obj, obj};
    const vector<vector<Vec2f>> p_pts = {p1, p2, p3};
    calibrateCamera(obj_pts, p_pts, args.imsz, K, D, rvecs, tvecs);

	//args.fovx= 2* atan2(K.at<double>(0,2), K.at<double>(0, 0))* _180overpi;
	//args.fovy = 2 * atan2(K.at<double>(1, 2),  K.at<double>(1, 1))* _180overpi;

    //-------- HOMOGRAPHY ----------------
    Mat R2;
    Rodrigues(rvecs[1], R2);
    Mat T2 = Mat(tvecs[1]);
    Mat tmp2;
    hconcat(vector<Mat>{R2.col(0), R2.col(1), T2}, tmp2);
    H2 = K * tmp2;
	const auto det2 = T2.at<double>(2, 0);
    if (abs(det2) < numeric_limits<double>::epsilon()*10.0) return MB_ERROR_NO_HOMOGRAPHY;
    H2 /= det2;

    Mat R3;
    Rodrigues(rvecs[2], R3);
    Mat T3 = Mat(tvecs[2]);
    Mat tmp3;
    hconcat(vector<Mat>{R3.col(0), R3.col(1), T3}, tmp3);
    H3 = K * tmp3;
	const auto det3 = T3.at<double>(2, 0);
    if (abs(det3) < numeric_limits<double>::epsilon()*10.0) return MB_ERROR_NO_HOMOGRAPHY;
    H3 /= det3;


    return MB_ERROR_OK;
}

//--------------------------------------------------------------------------------------------------
/// Routine che estrae i vertici del foglio A4 dall'immagine
/// Riempie la struttura Process_A4_Args_s con i risultati
static int process_for_a4(Process_A4_Args_s &args) {

    auto &im = args.im;

    //--- PREPROCESSING --------------------------------
    /// Viene fatto un piccolo preprocessing:
    /// bilanciamento del colore e blur 3x3
    im = color_balance(im);
    blur(im, im, {3,3});
    if (args.params->debug >= 3) imsave(im, "Balanced");

    //--- FOGLIO A4 ------------------------------------
    /// Si estrae il foglio A4 mediante thresholding
    /// usando l'allgoritmo Intermodes.
    /// Poi si fittano delle rette lungo i lati del foglio per individuare la posizione
    /// precisa dei suoi vertici
    /// Se serve, l'immagine viene flippata dx/sx
    auto a4_mask = extract_A4(im, args.params);
    auto a4_vertices = find_a4_corners(im, a4_mask, args.params);
    if (a4_vertices.size() != 4) return MB_ERROR_NO_A4;
    flip_image(a4_vertices, a4_mask, im, args.params);
	swap(a4_vertices[2], a4_vertices[3]);
    args.a4 = {im, a4_vertices, a4_mask};
    return MB_ERROR_OK;
}

//--------------------------------------------------------------------------------------------------
static Profile_s process_profile(const vector<Point> &pp, const AlgoParams *params) {

    const int T = params->heel_y_search_step;

    // linearly interpolate to fill holes
    const int y0 = pp[0].y;
    const int yn = pp[pp.size()-1].y;
    const int n = (yn - y0) / T + 1;
    vector<double> Y;
    vector<double> X;
    vector<double> DX;
    Y.reserve(n);
    X.reserve(n);
    DX.reserve(n-1);
    X.push_back(pp[0].x);
    Y.push_back(pp[0].y);
    for (size_t i=1; i<pp.size(); ++i) {
        const int dy = pp[i].y - pp[i-1].y;
        double dx = pp[i].x - pp[i-1].x;
        if (dy > T) {
            const int m = dy / T;
            dx /= m;
            for (int j=0; j<m; ++j) {
                X.push_back(pp[i-1].x + j*dx);
                Y.push_back(pp[i-1].y + (j+1)*T);
                DX.push_back(dx);
            }
        }
        else {
            X.push_back(pp[i].x);
            Y.push_back(pp[i].y);
            DX.push_back(dx);
        }
    }

    // smooth derivative with gaussian
    const vector<double> k = {0.00081408,   0.00383664,   0.01159686,   0.02689221,   0.05100139,   0.08183405,   0.11339781,
                        0.13738071,   0.14648851,   0.13738071,   0.11339781,   0.08183405,   0.05100139,   0.02689221,
                        0.01159686,   0.00383664,   0.00081408};
    auto DX2 = convolve(DX, k);

    // rebuild profile using smoothed derivative
    vector<double> X2(n);
    X2[0] = 0;
    for (int i=1; i<n; ++i) {
        X2[i] = X2[i-1] + DX2[i-1];
    }

    // compute new derivative
    vector<double> DX3(n-1);
    for (int i=1; i<n; ++i) {
        DX3[i-1] = X2[i] - X2[i-1];
    }

    // average new derivative
    const vector<double> k2 = {1.0/11.0, 1.0/11.0, 1.0/11.0, 1.0/11.0, 1.0/11.0, 1.0/11.0, 1.0/11.0, 1.0/11.0, 1.0/11.0, 1.0/11.0, 1.0/11.0};
    auto DX4 = convolve(DX3, k2);

    // rebuild profile using averaged derivative
    vector<double> X3(n);
    X3[0] = 0;
    for (int i=1; i<n; ++i) {
        X3[i] = X3[i-1] + DX4[i-1];
    }
    double sX = 0;
    double sX3 = 0;
    for (int i=0; i<n; ++i) {
        sX += X[i];
        sX3 += X3[i];
    }
    for (int i=0; i<n; ++i) {
        X3[i] += (sX-sX3)/n;
    }

    // look for zero-crossings in derivative
    vector<int> zeros;
    for (int i=params->heel_min_number_of_points/2; i<n-params->heel_min_number_of_points/2; ++i) {
        auto &d = DX4;
        if (d[i-1] < 0.0 && d[i] > 0.0) zeros.push_back(i);
    }

    vector<Point> heel_pts;
    for (auto i : zeros) {
        const Point p(lround(X3[i]+0.5), lround(Y[i]+0.5));
        heel_pts.push_back(p);
    }

    if (zeros.empty()) {
        const int ws = 11;
        const int h = ws / 2;

        auto &d = DX4;
        double mv = 1e10;
        int mi = -1;
        for (int i=h; i<n-h-1; ++i) {
            int m = 1;
            for (int k=-h; k<=h; ++k) {
                if (k!=0 && d[i+k]<d[i]) {
                    m = 0;
                    break;
                }
            }
            if (m && d[i]<mv) {
                mv = d[i];
                mi = i;
            }
        }

        if (mi > 0) {
            const Point p(lround(X3[mi]+0.5), lround(Y[mi]+0.5));
            heel_pts.push_back(p);
        }
    }

    vector<Point> profile(n);
    for (int i=0; i<n; ++i) {
        profile[i] = Point(lround(X3[i]+0.5), lround(Y[i]+0.5));
    }

	return { profile, heel_pts };
}

//--------------------------------------------------------------------------------------------------
/// Calcola la posizione della punta del piede, ed il profilo del tallone
static int process_for_foot(Process_Foot_Args_s &args) {

    auto &im = args.a4.im;
    auto &a4_mask = args.a4.a4_mask;
    auto &a4_vertices = args.a4.vertices;
    const auto params = args.params;

    args.toe = toe_point(im, a4_mask, a4_vertices, params);
    auto candidate_profile = heel_points(im, a4_mask, a4_vertices, params);
    if (args.toe.x < 0 || args.toe.y < 0 || candidate_profile.size() < params->heel_min_number_of_points) return MB_ERROR_NO_FOOT;

    auto profile = process_profile(candidate_profile, params);
    if (profile.heel_pts.empty()) return MB_ERROR_NO_FOOT;

    swap(profile.heel_pts, args.heel_pts);
    swap(profile.profile, args.heel_profile);
    swap(candidate_profile, args.heel_c_profile);

    return MB_ERROR_OK;
}

//--------------------------------------------------------------------------------------------------
int process(const char *image_above_fname, const char *image_external_fname, const char *image_internal_fname, const AlgoParams *params, Passing_data *passaggio_data) {

	if (params->debug >= 2) {
		set_imsave_path(params->debug_save_path);
	}

	//---- LOAD IMAGES -------------------------------------------------
	/// Carica le immagini, le traspoone se necessario (e forse conviene ridimensionarle
	/// tutte ad una dimensione di default, as QXGA)
	Mat im1;
	Mat im2;
	Mat im3;
	if (imload_and_resize(image_above_fname, im1, params)) return MB_ERROR_BAD_IMAGE;
	if (imload_and_resize(image_external_fname, im2, params)) return MB_ERROR_BAD_IMAGE;
	if (imload_and_resize(image_internal_fname, im3, params)) return MB_ERROR_BAD_IMAGE;
	if (im1.size != im2.size || im1.size != im3.size) return MB_ERROR_IMAGE_SZ_DISAGREE;



	const clock_t t0 = clock();


	//---- FOGLIO A4 ---------------------------------------------------
	/// Adesso individuiamo il foglio A4
	/// La routine process_for_a4 su tre thread, ciascuno per ogni immagine
	/// e le strutture "Process_A4_Args_s", passate per puntatore, conterranno
	/// il risultato dell'elaborazione: vettore dei 4 vertici e maschera binaria di
	/// segmentazione del foglio A4
	/// Alla fine aspettiamo che i tre thread ritornino per procedere.
	Process_A4_Args_s a4_1 = { im1, params,{ {},{},{} } };
	Process_A4_Args_s a4_2 = { im2, params,{ {},{},{} } };
	Process_A4_Args_s a4_3 = { im3, params,{ {},{},{} } };
    auto th1 = std::async([&a4_1]() { return process_for_a4(a4_1); }); // NOTE: Do not transform this into std::async(process_for_a4, a4_1);, as in this way process_for_a4 wouldn't receive the parameter by non-const reference (but rather by copy/move). 
    auto th2 = std::async([&a4_2]() { return process_for_a4(a4_2); }); //       Side effects are lost with the forwarding version of std::async - consider a monadic version.
    auto th3 = std::async([&a4_3]() { return process_for_a4(a4_3); });
	const auto ret1 = th1.get();
	const auto ret2 = th2.get();
	const auto ret3 = th3.get();
	if (params->debug >= 1) cout << "Foglio A4: " << ret1 << " " << ret2 << " " << ret3 << endl;
	if ((ret1 + ret2 + ret3) != MB_ERROR_OK) return MB_ERROR_NO_A4;

	//---- CALIBRAZIONE ------------------------------------------------
	/// Ora in una altro thread viene lanciata la routine calibrate_camera_and_retrieve_poses
	/// che usando le posizioni dei 4 vertici stima la matrice intrinseca della fotocamera e le pose
	/// della fotocamera nelle tre immagini
	/// Non serve attendere subito che il thread ritorni
	Process_Calibrate_Args_s ac = { im1.size(), a4_1.a4.vertices, a4_2.a4.vertices, a4_3.a4.vertices };

    auto thc = std::async([&ac]() { return calibrate_camera_and_retrieve_poses(ac); });



	//---- PIEDE -------------------------------------------------------
	/// Contemporaneamente lanciamo su due thread la routine process_for_foot che ricerca
	/// il profilo del tallone nelle due immagini laterali
	Process_Foot_Args_s af_2 = { a4_2.a4, params,{},{} };
	Process_Foot_Args_s af_3 = { a4_3.a4, params,{},{} };
    auto th2a = std::async([&af_2]() { return process_for_foot(af_2); });
    auto th3a = std::async([&af_3]() { return process_for_foot(af_3); });
    const auto ret2a = th2a.get();
	const auto ret3a = th3a.get();
	if (params->debug >= 1) cout << "Piede: " << ret2 << " " << ret3 << endl;
	if ((ret2a + ret3a) != MB_ERROR_OK) return MB_ERROR_NO_FOOT;

	//---- CALIBRAZIONE ------------------------------------------------
	/// Attendiamo la fine della routine di calibrazione...
	const auto retc = thc.get();
	if (params->debug >= 1) cout << "Calibrazione: " << retc << endl;
	if (retc != MB_ERROR_OK) return MB_ERROR_NO_HOMOGRAPHY;


	//---- LUNGHEZZA PIEDE ---------------------------------------------
	/// Ora conosciamo le pose delle telecamere, la matrice omografica delle due viste
	/// laterali, la posizione della punta del piede, la posizione del punto del tallone,
	/// così stimiamo la lunghezza in mm del piede
	auto &h2 = af_2.heel_pts;
	auto &h3 = af_3.heel_pts;
	if (h2.size() > 1 || h3.size() > 1) return MB_ERROR_INDECISION;
	double len2, len3;
	vector<Point2d> ppoints2, ppoints3;
	{
		const vector<Point2d> points = { af_2.toe, af_2.heel_pts[0] };
		vector<Point2d> upoints;
		vector<Vec3d> unprj;
		undistortPoints(points, upoints, ac.K, ac.D, noArray(), ac.K);
        vector<Vec3d> upsv;
        upsv.reserve(upoints.size());
        for (const auto &p : upoints) upsv.emplace_back(p.x, p.y, 1.0);
		Mat Hinv;
		invert(ac.H2, Hinv);
		for (const auto &p: upsv) {
			Mat pj = Hinv * Mat(p);
			pj /= pj.at<double>(2, 0);
			unprj.push_back(pj);
		}

		projectPoints(unprj, ac.R[1], ac.T[1], ac.K, ac.D, ppoints2);

		auto d = unprj[1] - unprj[0];
		len2 = sqrt(d[0] * d[0] + d[1] * d[1]);
	}
	{
		const vector<Point2d> points = { af_3.toe, af_3.heel_pts[0] };
		vector<Point2d> upoints;
		vector<Vec3d> unprj;
		undistortPoints(points, upoints, ac.K, ac.D, noArray(), ac.K);
        vector<Vec3d> upsv;
        upsv.reserve(upoints.size());
        for (const auto &p : upoints) upsv.emplace_back(p.x, p.y, 1.0);
		Mat Hinv;
		invert(ac.H3, Hinv);
		for (const auto& p : upsv) {
			Mat pj = Hinv * Mat(p);
			pj /= pj.at<double>(2, 0);
			unprj.push_back(pj);
		}

		projectPoints(unprj, ac.R[2], ac.T[2], ac.K, ac.D, ppoints3);

		auto d = unprj[1] - unprj[0];
		len3 = sqrt(d[0] * d[0] + d[1] * d[1] + d[2] * d[2]);
	}
	const auto dl = abs(len2 - len3);
	if (dl / len2 > 0.05) len2 = len3 = -1;
	cout << "Length: " << (len2 + len3) / 2.0 << endl;



	const clock_t t1 = clock();
	cout << "Elapsed ms " << t1 - t0 << endl;


	if (params->debug >= 2) {
		set_imsave_path(params->debug_save_path);
		draw_vertices(im1, a4_1.a4.vertices, { 0, 0, 255 }, -1, true);
		draw_vertices(im2, a4_2.a4.vertices, { 0, 0, 255 }, -1, true);
		draw_vertices(im3, a4_3.a4.vertices, { 0, 0, 255 }, -1, true);

		circle(im2, af_2.toe, 5, { 0,0,255 }, -1, LINE_AA);
		circle(im3, af_3.toe, 5, { 0,0,255 }, -1, LINE_AA);

        draw_line_strip(im2, af_2.heel_profile);
        draw_line_strip(im3, af_3.heel_profile);
        draw_vertices(im2, af_2.heel_pts, { 0, 0, 255 }, -1, false);
        draw_vertices(im3, af_3.heel_pts, { 0, 0, 255 }, -1, false);

		

		// Verifica correttezza delle matrici di posa
        const vector<Point3d> points = { {0.0,0.0,0.0}, {210.0,0.0,0.0}, {0.0,-297.0,0.0}, {210.0,-297.0,0.0} };
		vector<Point2d> ppoints;
		projectPoints(points, ac.R[0], ac.T[0], ac.K, ac.D, ppoints);
        draw_vertices(im1, ppoints, { 255, 0, 0 }, 2, false);
		projectPoints(points, ac.R[1], ac.T[1], ac.K, ac.D, ppoints);
        draw_vertices(im2, ppoints, { 255, 0, 0 }, 2, false);
        projectPoints(points, ac.R[2], ac.T[2], ac.K, ac.D, ppoints);
        draw_vertices(im3, ppoints, { 255, 0, 0 }, 2, false);


		imsave(im1);
		imsave(im2);
		imsave(im3);
	}


	passaggio_data->ac = { ac };
	passaggio_data->a4_1 = { a4_1.a4 };
	passaggio_data->a4_2 = { a4_2.a4 };
	passaggio_data->a4_3 = { a4_3.a4 };
	passaggio_data->im2_heel_c_profile = { af_2.heel_c_profile };
	passaggio_data->im2_heel_profile = { af_2.heel_profile };
	passaggio_data->im2_heel_pts = { af_2.heel_pts };
	passaggio_data->im3_heel_c_profile = { af_3.heel_c_profile };
	passaggio_data->im3_heel_profile = { af_3.heel_profile };
	passaggio_data->im3_heel_pts = { af_3.heel_pts };
	passaggio_data->warping = Mat::zeros(3, 3, CV_64F);
	
    const auto z = passaggio_data->ac.T[0].at<double>(2, 0);

	passaggio_data->ac.fovx = atan2(297.0/2.0, z)* _360overpi;
	passaggio_data->ac.fovy = atan2(210.0/2.0, z)* _360overpi;

    return MB_ERROR_OK;


}
