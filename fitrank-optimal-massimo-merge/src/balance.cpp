
#include "balance.h"
#include <opencv2/core/core.hpp>

using namespace std;
using namespace cv;

static void build_histogram(const Mat &mat, int (&hists)[4][256]) {
    memset(hists, 0, sizeof(hists));

    const int channels = mat.channels();
    assert(channels <= 4);
    // calcolo istogramma su 3 canali
    for (int y = 0; y < mat.rows; ++y) {
        const uchar* ptr = mat.ptr<uchar>(y);
        for (int x = 0; x < mat.cols; ++x) {
            for (int i = 0; i < channels; ++i) {
                hists[i][*ptr++]++;
            }
        }
    }
}

static void scaleAndOffset(Mat &mat, float(&offset)[4], float(&scale)[4]) {
    assert(mat.depth() == CV_8U);
    const int channels = mat.channels();
    for (int y = 0; y < mat.rows; ++y) {
        uchar* ptr = mat.ptr<uchar>(y);
        for (int x = 0; x < mat.cols; ++x) {
            for (int i = 0; i < channels; ++i) {
                *ptr = saturate_cast<uchar>(int((*ptr + offset[i]) * scale[i])); // PAN: Force int-conversion to use truncation rather than rounding (for compatibility with original code - investigate rounding better)
                ++ptr;
            }
        }
    }

}

void autoLevels(Mat &mat) { // Esecuzione in 34ms. Accettabile, eseguito solo all'inizio.
    constexpr double discard_ratio = 0.05;
    int hists[4][256];
    build_histogram(mat, hists);

    // calculate scale and offset directly on the histograms (by accumulating channel by channel)
    const int total = mat.cols*mat.rows;
    const int discard_low = int(discard_ratio * total);
    const int discard_high = total - discard_low;
    float offset[4], scale[4];
    for (int i = 0; i < mat.channels(); ++i) {
        int currMin = 0;
        int currMax = 255;
        int acc = hists[i][0];
        for (int j = 1; j < 255; ++j) {
            acc += hists[i][j];
            if (acc < discard_low) {
                currMin = j + 1;
            } else if (acc > discard_high) {
                currMax = j;
                break;
            }
        }
        offset[i] = float(-currMin);
        scale[i] = 255.f / (currMax - currMin);
    }

    scaleAndOffset(mat, offset, scale);
}

void sigmoide_contrasto(const Mat& src, Mat& dst, float E, float m) {  // Ottimizzato, esecuzione in 4ms
    Mat lut(1, 256, CV_8U);
    for (int i = 0; i < 256; i++)
        lut.at<uchar>(i) = saturate_cast<uchar>(255.0f / (1.0f + pow(m / i, E)));
    LUT(src, lut, dst);
}

static void computeMinMaxValuesAuto(const Mat& im, int* min, int* max)
{
    // wiki.cmci.info/documents/120206pyip_cooking/python_imagej_cookbook#automatic_brightnesscontrast_button
    std::vector<unsigned long long> hist(256, 0);

    for (int y = 0; y < im.rows; ++y)
    {
        uchar* ptr = im.data + y * im.step;
        for (int x = 0; x < im.cols; ++x)
        {
            ++hist[ptr[x]];
        }
    }

    const unsigned long long limit = im.cols * im.rows / 10;
    const unsigned long long threshold = im.cols * im.rows / 5000;

    int i = -1;
    while (true)
    {
        ++i;
        auto count = hist[i];
        if (count > limit)
        {
            count = 0;
        }
        if ((count > threshold) || i >= 255)
        {
            break;
        }
    }
    *min = i;
    i = 256;
    while (true)
    {
        --i;
        auto count = hist[i];
        if (count > limit)
        {
            count = 0;
        }
        if ((count > threshold) || i < 1)
        {
            break;
        }
    }
    *max = i;
}

static void do_enhance(Mat& im, int min, int max)
{
    for (int y = 0; y < im.rows; ++y)
    {
        uchar* ptr = im.data + y * im.step;
        for (int x = 0; x < im.cols; ++x)
        {
            ptr[x] = saturate_cast<uchar>(((ptr[x] - min) * 255) / (max - min));
        }
    }
}

static void contrast_enhance(Mat& im)
{
    int min, max;
    computeMinMaxValuesAuto(im, &min, &max);
    do_enhance(im, min, max);
}

Mat color_balance(const Mat& im)
{
    Mat b;
    vector<Mat> ch;
    split(im, ch);

    for (auto& c : ch) contrast_enhance(c);
    merge(ch, b);

    return b;
}

static bool bimodal_test(const std::vector<double>& y)
{
    int modes = 0;
    for (size_t k = 1; k < y.size() - 1; ++k)
    {
        if (y[k - 1] < y[k] && y[k + 1] < y[k])
        {
            ++modes;
            if (modes > 2) return false;
        }
    }
    return (modes == 2);
}

static int intermodes(const Mat& im, const Mat* mask, bool cut_lower_histo)
{
    std::vector<double> histo(256, 0.0);
    if (mask)
    {
        for (int y = 0; y < im.rows; ++y)
        {
            uchar* ptr = im.data + y * im.step;
            uchar* mptr = mask->data + y * mask->step;
            for (int x = 0; x < im.cols; ++x)
            {
                if (mptr[x])
                {
                    const uchar v = ptr[x];
                    ++histo[v];
                }
            }
        }
    }
    else
    {
        for (int y = 0; y < im.rows; ++y)
        {
            uchar* ptr = im.data + y * im.step;
            for (int x = 0; x < im.cols; ++x)
            {
                const uchar v = ptr[x];
                ++histo[v];
            }
        }
    }
    if (cut_lower_histo)
    {
        for (int i = 0; i < 80; ++i)
        {
            histo[i] = 0;
        }
    }

    int iter = 0;
    while (!bimodal_test(histo))
    {
        auto current = 0.0;
        auto next = histo[0];

        for (size_t i = 0; i < histo.size() - 1; ++i)
        {
            const auto previous = current;
            current = next;
            next = histo[i + 1];
            histo[i] = (previous + current + next) / 3.0;
        }
        histo[histo.size() - 1] = (current + next) / 3.0;
        ++iter;
        if (iter > 5000)
        {
            return -1;
        }
    }

    int tt = 0;
    for (size_t i = 1; i < histo.size() - 1; ++i)
    {
        if (histo[i - 1] < histo[i] && histo[i + 1] < histo[i])
        {
            tt += i;
        }
    }
    return tt / 2;
}

void do_threshold(const Mat& im, Mat& bw)
{
    if (bw.cols != im.cols || bw.rows != im.rows) bw = Mat(im.size(), CV_8U);

    std::vector<Mat> channels;
    split(im, channels);
    const size_t n = channels.size();
    std::vector<int> th_v(n);

    for (size_t i = 0; i < n; ++i)
    {
        th_v[i] = intermodes(channels[i], nullptr, i == 0); // cut lower histo on blue channel
    }

    for (int y = 0; y < im.rows; ++y)
    {
        const auto ptr = im.data + y * im.step;
        const auto bwptr = bw.data + y * bw.step;
        for (int x = 0; x < im.cols; ++x)
        {
            const auto v = ptr + n * x;
            uchar mv = 255;
            for (size_t i = 0; i < n; ++i)
            {
                if (v[i] < th_v[i])
                {
                    mv = 0;
                }
            }
            bwptr[x] = mv;
        }
    }
}

Mat do_threshold(const Mat& im)
{
    Mat bw(im.rows, im.cols, CV_8U);
    do_threshold(im, bw);
    return bw;
}
