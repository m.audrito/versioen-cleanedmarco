#include "addi-fit2_ottimizzato.h"
#include "balance.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <cmath>
#include <bitset>
#include <iterator>

#include "loadobj.h"
#include "utility.h"
#include "render.h"
#include "mb_main.h"

using namespace cv;
using namespace std;


template<class ForwardIt, class Value, class Fn>
ForwardIt min_element_fn_threshold(const ForwardIt first, const ForwardIt last, const Value threshold, Fn fn)
{
	ForwardIt smallest = last;
	Value smallestValue{};
	for (ForwardIt current = first; current != last; ++current) {
		auto currentValue = fn(*current);
		if (currentValue > threshold && (smallest == last || currentValue < smallestValue)) {
			smallest = current;
			smallestValue = currentValue;
		}
	}
	return smallest;
}

template<class ForwardIt, class Fn>
ForwardIt max_element_fn(const ForwardIt first, const ForwardIt last, Fn fn)
{
	ForwardIt largest = last;
	std::decay_t<decltype(fn(*first))> largestValue{};
	for (ForwardIt current = first; current != last; ++current) {
		auto currentValue = fn(*current);
		if (largest == last || largestValue < currentValue) {
			largest = current;
			largestValue = currentValue;
		}
	}
	return largest;
}

template<class ForwardIt, class Fn>
ForwardIt min_element_fn(const ForwardIt first, const ForwardIt last, Fn fn)
{
	ForwardIt smallest = last;
	std::decay_t<decltype(fn(*first))> smallestValue{};
	for (ForwardIt current = first; current != last; ++current) {
		auto currentValue = fn(*current);
		if (smallest == last || currentValue < smallestValue) {
			smallest = current;
			smallestValue = currentValue;
		}
	}
	return smallest;
}

/**  
 * Estrae la sagoma del piede e del foglio partendo da una foto.
 * @param nomefile Nome del file contente la foto da elaborare.
 * @param flagimg Variabile per il salvataggio dei flag descrittivi dell’immagine.
 * @return Sagoma e Indicatore di qualità dell’immagine o -1 in caso di fallimento.
 */
std::pair<vector<Point>, int> estrazione_sagoma(Mat in_image, int *flagimg, int width, int height) {

   
	auto input_image = in_image;
    if ((input_image.cols < width) || (input_image.rows < height)) {
        return { {}, -1 };
    }
	
    autoLevels(input_image); //34ms in esecuzione
   // medianBlur(input_image, input_image, 9); //144ms in esecuzione

    //detect blur
    Mat grayscale_image;
    cvtColor(input_image, grayscale_image, CV_BGR2GRAY);

    Mat temporary_image;
    Laplacian(grayscale_image, temporary_image, CV_64F);
    Scalar mean, stddev; //0:1st channel, 1:2nd channel and 2:3rd channel
    meanStdDev(temporary_image, mean, stddev, Mat());
    if (stddev.val[0] * stddev.val[0] < 11) {
        *flagimg |= 1;
    }

    int conta_iterazioni = 0;

    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;
    for (int gamma = MIN_E; gamma < MAX_E; ++gamma) {
        for(int m = MIN_M; m < MAX_M; ++m) {
            sigmoide_contrasto(input_image, temporary_image, gamma - 100.f, float(m)); //durata 4ms
			
	
            cvtColor(temporary_image, grayscale_image, CV_BGR2GRAY); //durata 3ms

            Canny(grayscale_image, grayscale_image, 7 * 10, 7 * 22); //durata 3ms (compreso dilate ed erode)
            dilate(grayscale_image, grayscale_image, Mat());
            erode(grayscale_image, grayscale_image, Mat());

            findContours(grayscale_image, contours, hierarchy, CV_RETR_LIST, CHAIN_APPROX_NONE); //durata 1.2ms
            const auto minAreaAboveThresholdIt = min_element_fn_threshold(contours.begin(), contours.end(), grayscale_image.total() / 20.0, [](auto &&r) {return contourArea(r); });

		//	cout << " area contorno  " << minAreaAboveThresholdIt. << " contour_end  " << contours.end() << endl;
			if (minAreaAboveThresholdIt != contours.end()) {
                return { move(*minAreaAboveThresholdIt), conta_iterazioni };
            }
            conta_iterazioni++;
        }
    }

    *flagimg |= 2;
    return {{}, -1};
}


/// Funzione di visualizzazione su schermo
/**
* VIsualizza su schermo l'immagine .
* @param img immagine.
* @param str intestazione immagine

*/
void Visualizza(const Mat img, string str)
{
	namedWindow(str, WINDOW_NORMAL);
	resizeWindow(str, 600, lround(600 * img.rows / img.cols));
	imshow(str, img);
	waitKey(1);
}

void fillSinglePoly(Mat &image, const vector<Point> &contour, const int color, const Point &offset = Point())
{
    auto x = contour.data();
    auto sz = int(contour.size());
    fillPoly(image, &x, &sz, 1, color, 8, 0, offset);
}

/**
 * Analizza i punti di una sagoma ed estrae i vertici del foglio.
 * @param paper_and_foot_path   Vettore di punti della sagoma da analizzare.
 * @param flagimg               Variabile per il salvataggio dei flag descrittivi dell’immagine.
 * @return Vertici (4) o array vuoto in caso di errore
 */
vector<Point> estrazione_vertici(const vector<Point>& paper_and_foot_path, int *flagimg, int width, int height) { // estrae i vertici, molto rapida e stabile
    Mat imgsagoma;

    RotatedRect rettangolo = minAreaRect(paper_and_foot_path);
    vector<Point2f> vertici(4);
    rettangolo.points(&vertici[0]);

    vector<Point> hull;
    convexHull(paper_and_foot_path, hull, false, true);

    // Individuazione vertici

    auto distamin = std::numeric_limits<double>::max();
    auto distbmin = std::numeric_limits<double>::max();
    auto distcmin = std::numeric_limits<double>::max();
    auto distdmin = std::numeric_limits<double>::max();
    vector<Point> vert(4);
    for (auto& point : hull)
    {
        const Point2f curPoint2f = point;

        const auto dista = distance(curPoint2f, vertici[0]);
        const auto distb = distance(curPoint2f, vertici[1]);
        const auto distc = distance(curPoint2f, vertici[2]);
        const auto distd = distance(curPoint2f, vertici[3]);

        if (dista <= distb && dista <= distc && dista <= distd && dista < distamin) {
            distamin = dista;
            vert[0] = point; //più vicino ad A
        }
        if (distb <= dista && distb <= distc && distb <= distd && distb < distbmin) {
            distbmin = distb;
            vert[1] = point; //più vicino a B
        }
        if (distc <= dista && distc <= distb && distc <= distd && distc < distcmin) {
            distcmin = distc;
            vert[2] = point; //più vicino a C
        }
        if (distd <= dista && distd <= distb && distd <= distc && distd < distdmin) {
            distdmin = distd;
            vert[3] = point; //più vicino a D
        }
    }

    //Verifica esattezza vertici trovati e qualità inquadratura
    //Elimina il quadrilatero e conta i punti bianchi. Se sono troppi un vertice è saltato
    {
        const auto area = contourArea(vert);
        //const float areaPre = contourArea(hull);

        Mat test_vertex(width, height, CV_8UC1);
        test_vertex = Scalar(0, 0, 0);
        fillSinglePoly(test_vertex, paper_and_foot_path, 255);
        fillConvexPoly(test_vertex, vert, 0);

        //
        const auto percentuale_foglio = area * 100.0 / (width*height);
        const auto percentuale_errore_vertici = countNonZero(test_vertex == 255)*100.0 / area;
        if (percentuale_foglio < 35.0)
        {
            *flagimg |= 4; 
        }
        if (percentuale_errore_vertici > 1.1)
        {
            *flagimg |= 8; 
            if (percentuale_errore_vertici > 3.0)
                return {}; //troppi pixel bianchi, errore vertici
        }
    }
    return vert;
}

template<typename T, typename U>
vector<T> vectorAsBigAs(const vector<U> &u)
{
    vector<T> result;
    result.resize(u.size());
    return result;
}

template<typename T, typename U>
vector<T> convert(const vector<U> &u)
{
    vector<T> result;
    result.reserve(u.size());
    std::copy(u.begin(), u.end(), std::back_inserter(result));
    return result;
}

/**
 * Applica una trasformazione prospettica all’immagine per uniformare il punto di vista delle diverse possibili immagini.
 * @param vertices Vettore contenente i vertici del foglio.
 * @param contour Sagoma da trasformare.
 * @return Sagoma trasformata, e la sua trasformazione.
 */
static tuple<vector<Point>, Mat> warpsagoma(const vector<Point2f> &vertices, const vector<Point> &contour) {
    const vector<Point2f> puntiwarp{
        { 0, IMG_HEIGHT_GL - 1 },
        { 0, 0 },
        { IMG_WIDTH_GL - 1, 0 },
        { IMG_WIDTH_GL - 1, IMG_HEIGHT_GL - 1 },
    };

    //Valuta orientamento del foglio

  
    assert(sqrDistance(vertices[0], vertices[1]) < sqrDistance(vertices[3], vertices[1]));
    const Mat M = getPerspectiveTransform(vertices, puntiwarp);
    auto transformed_contour = vectorAsBigAs<Point2f>(contour);
    transformed_contour.resize(contour.size());
    perspectiveTransform(convert<Point2f>(contour), transformed_contour, M);

    Mat sagoma_cut(IMG_HEIGHT_GL-2*CROP_GL, IMG_WIDTH_GL - 2 * CROP_GL, CV_8UC1);
    sagoma_cut = 255;

    // TODO: The following code just count the intersection between the contour and the top/bottom rectangle. Consider using a clipping library to avoid rendering.
    fillSinglePoly(sagoma_cut, convert<Point>(transformed_contour), 0, {-CROP_GL, -CROP_GL});


	// TODO: VAlutare se mantenere parte sottostante del codice 
    //Orientamento sagoma: la sagoma è nella metà foto con meno pixel bianchi. Considera come soglia una differenza del 15% del numero di pixel
    const auto contasu = countNonZero(sagoma_cut(Rect(0, 0, IMG_WIDTH_GL - 2 * CROP_GL, IMG_HEIGHT_GL / 2 - CROP_GL)) == 255);
    const auto contagiu = countNonZero(sagoma_cut(Rect(0, IMG_HEIGHT_GL / 2 - CROP_GL, IMG_WIDTH_GL - 2 * CROP_GL, IMG_HEIGHT_GL / 2 - CROP_GL)) == 255);

    if (contasu - contagiu > (contasu + contagiu) *15/100/2) { // rotate if difference is > 15% of average. Warning: this is biased toward maintaining the input orientation.
        flip(sagoma_cut, sagoma_cut, -1);
    }
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;
    findContours(sagoma_cut, contours, hierarchy, CV_RETR_LIST, CHAIN_APPROX_NONE); //durata 1.2ms
    const auto contourIt = min_element_fn_threshold(contours.begin(), contours.end(), sagoma_cut.total() / 10.0, [](auto &&r) {return contourArea(r); }); // PAN: Maintained implicit assumption: area exists (we should check the return of max_element_fn)
    if (contourIt != end(contours))
    {
        return { move(*contourIt), M };
    }
    return { vector<Point>(), M};
}



/**
* Calcola la posizione del piede sul foglio partendo da una ipotesi sulla rotazione e sulla dimensione.
* @param angolo Stima della rotazione del piede sul foglio.
* @param rettangolo Bounding box del contorno del piede trasformato.
* @param traslazione_modello Traslazione del modello 3D sul piano del foglio.
* @param main_object Modello 3D del piede virtuale.
* @param zopt, Coordinata Z per traslazione modello
* @param  zfinal Coordinata Z per traslazione modello
* @param scala Fattore di scala del modello 3D da applicare.
* @return  tuple con:
*                  momenti dell’immagine,
*                  bool indicante se contorno trovato
*                  area contorno
*                  */
tuple<Moments, bool, double> optimize(float angolo, const cv::Rect &rettangolo, float traslazione_modello, const Mesh &main_object, float zopt, float zfinal, float scala, float fovy, float z, float *scala_stimata = nullptr) {
	float incrementox = 0, incrementoy = 0;

	float scalaattuale = scala;
	// ciclo per ottimizzare la sovrapposizione  della sagoma proiettata attraverso Bounding box
	
	for (int i = 0; i < 50; i++) {
	

		const Rect_<float> r2 = calculateBoundingBox(main_object, traslazione_modello, angolo, incrementox, incrementoy, zopt, scalaattuale, fovy, z); // range -1 - 1
																																					// PAN: Scale and crop as needed. Use proper bbox intersection rather than this hand-baked version.
		const Rect rettangolo3(
			Point2i{ std::max(int(r2.tl().x), 0), std::max(int(r2.tl().y), 0) },
			Point2i{ std::min(int(r2.br().x), IMG_WIDTH_GL - 2 * CROP_GL - 1),  std::min(int(r2.br().y), IMG_HEIGHT_GL - 2 * CROP_GL - 1) }
		);
		// calcola le differenze in larghezza ed altezza per sovrapporre completamente i due rettangoli bounding box

		const auto deltax = (rettangolo3.y - rettangolo.y);
		const auto deltay = ((rettangolo3.x * 2 + rettangolo3.width) - (rettangolo.x * 2 + rettangolo.width));
		const auto deltascala = rettangolo3.width - rettangolo.width;
		if (deltax == 0 && deltay == 0 && ((!scala_stimata) || (deltascala == 0)))
			break;// sovrapposizione completa
				  // applica incremento per traslare il modello 
		incrementox += deltax / 2.0f; // / 2.0 is an arbitrary scale factor - this could be adjusted using the modelview/projection matrix
		incrementoy += deltay / 4.0f;
		if (scala_stimata) {
			scalaattuale = scalaattuale * rettangolo.width / rettangolo3.width;
		}
	}
	if (scala_stimata) {
		*scala_stimata = scalaattuale;
	}


	return calculatePose(main_object, traslazione_modello, angolo, incrementox, incrementoy, zfinal, scalaattuale, fovy, z);
}

/** DA VEDERE DI ELIMINARE IL NUOVO POSIZIONAMENTO!!!! 
// E un doppione di optimize....mi serve per calcolare proiezioni laterali....
TO DO: utlizzare optimize   in process con il passaggio degli incrementi!  e fare solo proiezione laterale
* @return  tuple con:
*                  momenti dell’immagine,
*                  bool indicante se contorno trovato
*                  area contorno
*				   bool proiezione laterale Se true all'interno della sagoma del piede utente!
*                  */

tuple<Moments, bool, double, bool> optimize_M(vector<Point> contour, float angolo, const cv::Rect &rettangolo, float traslazione_modello, const Mesh &main_object, float zopt, float zfinal, float scala, Passing_data &pass_data) {
	float incrementox = 0, incrementoy = 0;
    const auto fovy = float(pass_data.ac.fovy);
    const auto z = float(pass_data.ac.T[0].at<double>(2, 0));
    const float scalaattuale = scala;
	//cout << "\n Inizio ciclo scala:" << scala << "\n Inizio scala Stimata:" << scala_stimata << "\n";
	int temp_i = -1;
	for (int i = 0; i < 50; i++) {
		temp_i++;

		const Rect_<float> r2 = calculateBoundingBox(main_object, traslazione_modello, angolo, incrementox, incrementoy, zopt, scalaattuale, fovy, z); // range -1 - 1
																																					// PAN: Scale and crop as needed. Use proper bbox intersection rather than this hand-baked version.
		const Rect rettangolo3(
			Point2i{ std::max(int(r2.tl().x), 0), std::max(int(r2.tl().y), 0) },
			Point2i{ std::min(int(r2.br().x), IMG_WIDTH_GL - 2 * CROP_GL - 1),  std::min(int(r2.br().y), IMG_HEIGHT_GL - 2 * CROP_GL - 1) }
		);


		const auto deltax = (rettangolo3.y - rettangolo.y);
		const auto deltay = ((rettangolo3.x * 2 + rettangolo3.width) - (rettangolo.x * 2 + rettangolo.width));
		if (deltax == 0 && deltay == 0)
		{
				break;
		}
		incrementox += deltax / 2.0f; // / 2.0 is an arbitrary scale factor - this could be adjusted using the modelview/projection matrix
		incrementoy += deltay / 4.0f;

	}

	int puntatallone_index[2] = { 1381,266 }; //punta,tallone
	int sxdx_index[2] = { 1062,1616 };
	const auto current_mesh = scale(main_object, scalaattuale);
	const auto current_measured_ratio = distance(current_mesh.vertices(), puntatallone_index) / distance(current_mesh.vertices(), sxdx_index);
	//cout << "\n Inizio  ciclo scala:  " << scala << "\n FIne ciclo scala:  " << scalaattuale << " ATTUALE RAPPORTO LW:" << current_measured_ratio << "\n";
	
	calculatePose_M(contour, main_object, traslazione_modello, angolo, incrementox, incrementoy, zfinal, scalaattuale, fovy, z);
	 // Proietto modello in modo da veder se tallone sta all'interno dell'immagine
	//for (auto& i : contour)
	//{
 //       pass_data.contour_top_img.emplace_back(i.x, i.y, 0);
	//}
	bool check = ProjectModel(main_object, traslazione_modello, angolo, incrementox, incrementoy, zfinal, scalaattuale, pass_data);
	//const auto res = calculatePose(main_object, traslazione_modello, angolo, incrementox, incrementoy, zfinal, scalaattuale, fovy);
	const auto res = tuple_cat(calculatePose(main_object, traslazione_modello, angolo, incrementox, incrementoy, zfinal, scalaattuale, fovy, z), make_tuple(check));
	return res;
	
}

/**
 * Stima in modo grezzo la posizione ed il fattore di scala del piede basandosi sulla lunghezza e la larghezza occupate sul foglio.
 * @param angolo Angolo da assumere durante il rendering del modello 3D.
 * @param rettangolo Bounding box del contorno del piede trasformato.
 * @param traslazione_modello Traslazione del modello 3D sul piano del foglio.
 * @param main_object Modello 3D del piede virtuale.
 * @param scala_stimata Variabile per il salvataggio della scala stimata.
 * @return Momenti, momenti centrali e momenti centrali normalizzati dell’immagine.
 */
tuple<Moments, bool, double> stimainizialeposizionescala(float angolo, const cv::Rect &rettangolo, float traslazione_modello, const Mesh &main_object, float *scala_stimata, float fovy, float z) {
	return optimize(angolo, rettangolo, traslazione_modello, main_object, 9, 2.5, 1, fovy, z, scala_stimata);
}

/* Stima la rotazione del piede sul foglio individuando l’angolo associato
ad una massima similarità tra la sagoma elaborata e la sagoma virtuale.
* @param rettangolo circoscrive sagoma modello piede
* @param momenti  dall’immagine/foto ;
* @param traslazione del modello 3D sul piano del foglio;
* @param modello 3D del piede virtuale;
* @return angolo stimato.
*/
float correggirotazione(const Rect &rettangolo, const Moments &mu_orig, float traslazione_modello, const Mesh &main_object, float fovy, float z) {
    // PAN: Currently this calls 11 times the function stimainizialeposizionescala
    // TODO: Instead of three iterations with (maybe) a refining step, we can try different solutions:
    //       a) fit a parabola, and try to refine more from the estimated center (e.g. halving the step) - this will actually work even if the minima is expected to be outside the range (but could fail if the parabola is concave downwards)
    //       b) use a different minimisation function (e.g. fmins)

	//  valori di angolo in cui effettuare la ricerca
	float vetttest[5] = { -40,-20,0,20,40 };
	// array di punteggi per ogni range
	double punteggio[5] = { 0,0,0,0,0 };
	// val min e val massimo su cui effettuare la stima
	float angolo1 = -40, angolo2 = 40;
	float scala_stimata_dummy = 1;

	/* all'interno di vetttest[5]={-40,-20,0,20,40} ci sono i gradi/passi per stimare la rotazione del piede sul foglio
	si iniza a cercar nel range -40° e 40° suddividendolo in 5 passi (come in vett test) e si calcola punteggio quest'ultimo rappresentato dalla differenza dei momenti di inerzia immagine e proiezione modello sulla foto
	SI cercano i primi 2 minimi che rappresentano gli estremi di angoli su cui efettuare nuovamente la ricerca

	Es:
	-40 -20 0 20 40 ==> Minimini [0-20]==> 0 5 10 15 20 ==>Minimini [10-15]==>10 11,75 12 13,5 15
	Algoritmo dicotomico per trovar l’angolo con error massimo di +/- 0,655
	*/


	for (int i = 0; i < 3; i++) {
		//calcola un punteggio per ogni guess
		if (i == 0) {
			for (int j = 0; j < 5; j++) {
				const auto result = stimainizialeposizionescala(vetttest[j], rettangolo, traslazione_modello, main_object, &scala_stimata_dummy, fovy, z);
				if (get<1>(result))
				{
					const auto &tmp_moments = get<0>(result);
					punteggio[j] = abs(tmp_moments.nu02 - mu_orig.nu02) + abs(tmp_moments.nu03 - mu_orig.nu03) + abs(tmp_moments.nu11 - mu_orig.nu11) + abs(tmp_moments.nu12 - mu_orig.nu12) + abs(tmp_moments.nu20 - mu_orig.nu20) + abs(tmp_moments.nu21 - mu_orig.nu21) + abs(tmp_moments.nu30 - mu_orig.nu30);
				}
				else
				{
					punteggio[j] = numeric_limits<float>::max();
				}
			}
		}
		else
		{
			for (int j = 1; j < 4; j++) {
				const auto result = stimainizialeposizionescala(vetttest[j], rettangolo, traslazione_modello, main_object, &scala_stimata_dummy, fovy, z);
				if (get<1>(result))
				{
					const auto &tmp_moments = get<0>(result);
					punteggio[j] = abs(tmp_moments.nu02 - mu_orig.nu02) + abs(tmp_moments.nu03 - mu_orig.nu03) + abs(tmp_moments.nu11 - mu_orig.nu11) + abs(tmp_moments.nu12 - mu_orig.nu12) + abs(tmp_moments.nu20 - mu_orig.nu20) + abs(tmp_moments.nu21 - mu_orig.nu21) + abs(tmp_moments.nu30 - mu_orig.nu30);
				}
				else
				{
					punteggio[j] = numeric_limits<float>::max();
				}
			}
		}
		int idxmin = 0;
		double min = std::numeric_limits<double>::max();
		for (int j = 0; j < 5; j++) {
			if (punteggio[j] < min) {
				idxmin = j;
				min = punteggio[j];
			}
		}
		int idxproxmin = 0;
		double minprox = std::numeric_limits<double>::max();
		for (int j = 0; j < 5; j++) {
			if (punteggio[j] > min && punteggio[j] < minprox) {
				idxproxmin = j;
				minprox = punteggio[j];
			}
		}

		// PAN: When idxmin = 0 and idxproxmin = 4, this will actually not refine, but rebuild the exact same angles (passo = 20)

		const float passo = (vetttest[idxproxmin] - vetttest[idxmin]) / 4.0f;
		angolo1 = vetttest[idxmin];
		angolo2 = vetttest[idxproxmin];
		vetttest[0] = angolo1;
		vetttest[1] = angolo1 + passo;
		vetttest[2] = angolo1 + 2 * passo;
		vetttest[3] = angolo2 - passo;
		vetttest[4] = angolo2;
		punteggio[0] = punteggio[idxmin];
		punteggio[4] = punteggio[idxproxmin];
	}
	return (angolo1 + angolo2) / 2.0f;
}
/**
* Calcola la posizione del piede sul foglio partendo da una ipotesi sulla rotazione e sulla dimensione.
* @param scala Fattore di scala del modello 3D da applicare.
* @param angolo Stima della rotazione del piede sul foglio.
* @param rettangolo Bounding box del contorno del piede trasformato.
* @param traslazione_modello Traslazione del modello 3D sul piano del foglio.
* @param main_object Modello 3D del piede virtuale.
* @return mu Variabile per il salvataggio del momenti dell’immagine.
*/
tuple<Moments, bool, double> ottimizzaxy(float &scala, float angolo, const cv::Rect &rettangolo, float traslazione_modello, const Mesh &main_object, float fovy, float z) {
	return optimize(angolo, rettangolo, traslazione_modello, main_object, 9, 9, scala, fovy, z);
}


/**
* //controllo sulla posizione del piede all'interno del foglio restituisce se piede avanti o indietro mediante flag
Indica se piede è avanti o tropo indietro
* @param contour: Contorno Sagoma Immagine.
* @param posizione_alfa angolo della rotazione del modello del piede sul foglio.
* @param flagimg: byte dei flag sull'immagine de trasformato.
*/
void controlloposizione(const vector<Point> &contour, int width, int height, float posizione_alfa, int *flagimg)
{
    //controllo sulla posizione del piede all'interno del foglio
	vector<Point> rotated_contour;
	rotated_contour.resize(contour.size());
	// Applica rotazione del contorno sagoma immagine di un angolo posizione_alfa
	const Mat r = getRotationMatrix2D(Point2f(width / 2.f, height / 2.f), -posizione_alfa, 1.f);
	transform(contour, rotated_contour, r);

	// Mantiene le dimensioni immagine 1200x900
	const Rect clippingRegion{ 0, 0, width, height };

	const Rect rettangolo_test_posizione = boundingRect(rotated_contour) & clippingRegion;

	// Controllo posizione del piede sul foglio
	if (float(rettangolo_test_posizione.width) / rettangolo_test_posizione.height > 0.78)
	{
		*flagimg |= 32; // posizione troppo avanti
	}
	if (float(rettangolo_test_posizione.width) / rettangolo_test_posizione.height < 0.68)
	{
		*flagimg |= 16; // posizione troppo indietro
	}
}



/*
Calcola il fattore di scala del modello in grado di massimizzare il matching con la sagoma trasformata.
* attraverso un array di lunghezze possibili da dimensioneMin (220 mm)a dimensione max (330 mm)
*/
/**
* Calcola il fattore di scala del modello in grado di massimizzare il matching con la sagoma trasformata.
* Questo avviene scalando il modello 3D a rapporto costante di lunghezza / larghezza ma su un range di lunghezze tra il minimo ed il massimo
* @param rettangolo Bounding box del contorno del piede trasformato.
* @param mu_orig momenti originali del contorno del piede.
* @param flagimg Variabile per il salvataggio dei flag descrittivi dell’immagine.
* @param puntatallone_index Vettore contenente l’indice dei punti di interesse del modello (punta, tallone).
* @param main_object Modello 3D del piede virtuale.
* @param traslazione_modello Traslazione del modello 3D sul piano del foglio.
* @param sagoma_warp_cut Immagine della sagoma trasformata.
* @param posizione_alfa Angolo di rotazione del piede stimato.
* @param scala_stimata Variabile per il salvataggio della scala stimata.
* @return Codice di uscita 3 in caso di fallimento e 0 in caso di successo.
*/


float ottimizzascala2_fast( const Rect &rettangolo, const Mesh &main_object, float traslazione_modello, float posizione_alfa, float fovy, float z) {
    float scala_stimata;
	optimize(posizione_alfa, rettangolo, traslazione_modello, main_object, 9, 9, 1, fovy, z, &scala_stimata);
    return scala_stimata;
}


/*
Applica scala al modello e calcola le distanze tra i vertici rappresentati i  punti anatomici
* @param modello_piede: Mesh modello 3D.
* @param puntatallone_index: indici dei vertici punta tallone
* @param sxdx_index:indici dei vertici larghezza piede
* @param scala: scala da applicare al modello 3D
* @param dimensioni: array contenete le distanze calcolate.
*/
void dimensioni_applicate_scala(const Mesh &modello_piede, const int(&puntatallone_index)[2], const int(&sxdx_index)[2], float scala, float(&dimensioni)[4]) {
    int indicesCollo[] = { 1544, 710, 627, 984, 1008, 1107, 1121, 1206, 1239, 1531, 1502, 1691, 1560, 1544 };
    const double collopiede = distance(modello_piede.vertices(), indicesCollo);
    const double puntatallone = distance(modello_piede.vertices(), puntatallone_index);
    const double lunghezzapiede = abs(modello_piede.vertices()[puntatallone_index[0]].x - modello_piede.vertices()[puntatallone_index[1]].x); // PAN: Just deltaX of the above
    const double larghezzapiede = distance(modello_piede.vertices(), sxdx_index);

    dimensioni[0] = float(lunghezzapiede * scala);
    dimensioni[1] = float(larghezzapiede * scala);
    dimensioni[2] = float(collopiede * scala);
    dimensioni[3] = float(puntatallone * scala);
}

void salva_output_3D(const Mesh &mesh, const string &nomefile, float scala) {
    save_obj(mesh, nomefile.c_str(), scala);
}

/**
* Funzione di estrazione della sagoma e dei vertici, infine correzioen prospettica.
* @param nomefile: Nome della foto da elaborare.
* @param flagimg: Variabile per il salvataggio dei flag descrittivi dell’immagine.
* @param width: variabile larghezza minima del foglio 1200
* @param height: variabile lunghezza minima del foglio 900
* @param qualitaimg: Variabile per il salvataggio dell’indicatore di qualità della foto.
* @return contorno sagoma immagine, e la relativa trasformazione.
*/
static tuple<vector<Point>, Mat> load_and_trace(const Mat img, int *flagimg, int width, int height, int *qualitaimg, const vector<Point2f> &vertices)
{
    Mat sagoma_img;
    /* FASE 1: estrazione della sagoma. */
    const auto result = estrazione_sagoma(img, flagimg, width, height);
    const auto &sagoma_cnt = result.first;
    *qualitaimg = result.second;
    if (*qualitaimg >= 0)
    {
        auto w = warpsagoma(vertices, sagoma_cnt);
        return w;
        //}
    }
    return {};
}




/**
* Funzione che stima della rotazione del piede sul foglio e stima le dimensioni.
* @param contour: Contorno 


Sagoma Immagine.
* @param boundingRect: boundingBox del contorno.
* @param moments: Momenti dell'immagine .
* @param flagimg: byte di flag di errori.
* @param dimensione_ricercata_min: Minima dimensione da considerare nella ricerca.
* @param dimensione_ricercata_max: Massima dimensione da considerare nella ricerca.
* @param puntatallone_index: Vettore contenente l’indice dei punti di interesse del modello (punta-tallone)
* @param main_object: Modello 3D del piede virtuale.
* @param traslazione_modello: Traslazione del modello 3D sul piano del foglio.
* @param fovy: fov verticale
* @return posizione_alfa, scala_stimata.
*/
tuple<float, float> process(const vector<Point_<int>>& contour, const Rect& boundingRect, const Moments& moments, int *flagimg, const Mesh &main_object, float traslazione_modello, float fovy,  float z)
{
    /* FASE 4: stima della rotazione del piede sul foglio */
    const float posizione_alfa = correggirotazione(boundingRect, moments, traslazione_modello, main_object, fovy, z);

    /* FASE 5: stima delle dimensioni */
    controlloposizione(contour, IMG_WIDTH_GL - 2 * CROP_GL, IMG_HEIGHT_GL - 2 * CROP_GL, posizione_alfa, flagimg);

	/* Ottimizza posizione e scala */
    float scala_stimata = ottimizzascala2_fast(boundingRect, main_object, traslazione_modello, posizione_alfa, fovy, z);
    if ((*flagimg & 64) != 0)
    {
        return { 0.f, -1.f };//stima della scala è fallita a causa di una sagoma troppo differente dal modello
   
    }
    return { posizione_alfa, scala_stimata };
}


Mesh scaleY(const Mesh& mesh, const float current_y_scale)
{
	auto result = mesh;
	for (auto&p : result.vertices_)
	{
		p.y *= current_y_scale;
	}
	return result;
}

Mesh scale(const Mesh& mesh, const float current_scale)
{
	auto result = mesh;
	for (auto&p : result.vertices_)
	{
		p *= current_scale;
	}
	return result;
}

/* Modifica della funzione projectPoints in render.cpp*/
Point3f MoveModelPoint3D(const Point3f &point, const Mat &matrix)
{
	const float *mm = reinterpret_cast<float*>(matrix.data);
	const auto w = double(mm[3]) * point.x + double(mm[7]) * point.y + double(mm[11]) * point.z + mm[15];
	const auto x = (double(mm[0]) * point.x + double(mm[4]) * point.y + double(mm[8]) * point.z + mm[12]) / w;
	const auto y = (double(mm[1]) * point.x + double(mm[5]) * point.y + double(mm[9]) * point.z + mm[13]) / w;
	const auto z = (double(mm[2]) * point.x + double(mm[6]) * point.y + double(mm[10]) * point.z + mm[14]) / w;

	return{ float(x), float(y),float(z) };
}

Mesh Scale_translate_Model(const Mesh& mesh, Mat mat)
{
	auto result = mesh;
	for (auto&p : result.vertices_)
	{
		
		p = MoveModelPoint3D(p, mat);
	}
	return result;
}

/**
* Funzione di elaborazione della singola foto.
Richiama in ordine le sezioni dell’algoritmo interessate e veicola le elaborazioni temporanee per completare il calcolo.
Obbiettivo: Estrarre la sagoma ed i vertici, trovare la scala da applicare al modello , confrontando i momenti della sagoma immagine ed il modello scalato ed traslato .
Creazione di un array di rapporti lunghezza e larghezza che rappresentano la variabilita nella morfologia di piedi.
Tali rapporti vengono applicati al modello 3D caricato e poi proiettati e confrontati con la sagoma immagine.

* @param nomefile Nome della foto da elaborare.
* @param flagimg Variabile per il salvataggio dei flag descrittivi dell’immagine.
* @param qualitaimg Variabile per il salvataggio dell’indicatore di qualità della foto.
* @param posizione_alfa Variabile per il salvataggio dell’angolo di rotazione stimato.
* @param puntatallone_index Vettore contenente l’indice dei punti di interesse del modello (punta-tallone)
* @param main_object Modello 3D del piede virtuale.
* @param traslazione_modello Traslazione del modello 3D sul piano del foglio.
* @param fovy fov verticale
* @return Scala stimata per l’immagine oppure codice -1 in caso di fallimento.
*/
pair<Mesh, float> elabora_immagine(const string &nomefile, int *flagimg, int *qualitaimg, int width, int height, float *posizione_alfa, const int(&puntatallone_index)[2], const int(&sxdx_index)[2], const Mesh &main_object, float traslazione_modello, Passing_data &pass_data) {
	*posizione_alfa = 0;


	/*--------------------------MASSIMO-----------------------------------------------*/
	// Utilizzo la foto memorizzata in pass data della vista TOP per trovare la sagoma, utilizzando la mashera trovata da 
	// Massimo Borotlato per elaborare l'immagine del piede solo sul foglio, cio dovrebbe ridurre lo scarto di foto da parte di biocubica
	// dovuto all'influenza del pavimento
	

	//Visualizza(pass_data.a4_1.im,"immagine top");

	Mat img = pass_data.a4_1.im;
	//Mat img = pass_data.a4_1.a4_mask.mask;
	Mat mask = Mat::zeros(img.size(), CV_8UC1);
    vector<Point2i> sheet {
        pass_data.a4_1.vertices[0],
        pass_data.a4_1.vertices[1],
        pass_data.a4_1.vertices[2],
        pass_data.a4_1.vertices[3]
    };
	
	const Scalar Color = { 255,255,255 };
	fillConvexPoly(mask, sheet, Color);

	
	// Applico la maschera
	Mat outputMat;
	img.copyTo(outputMat, mask);

  
	const auto contour_data = load_and_trace(outputMat, flagimg, width, height, qualitaimg, pass_data.a4_1.vertices);
    auto &contour = get<0>(contour_data);
	if (contour.empty())
	{
		return{ main_object, -1.f };
	}

    pass_data.warping = get<1>(contour_data);

	//calcola i momenti di inerzia sull'immagine originale
	const auto area = contourArea(contour, true);
	const auto rettangolo = boundingRect(contour);
	const Moments mu_orig = moments(contour, true);
	// range di valori per le proporzioni tra lunghezza e larghezza del piede
	constexpr auto min_w_h_ratio = 2.2;
	constexpr auto max_w_h_ratio = 3.2;
	// fattore di proporzione del modello
	const auto model_ratio = distance(main_object.vertices(), puntatallone_index) / distance(main_object.vertices(), sxdx_index);

	constexpr int N = 100;
	vector<tuple<float, float, double, double>> records; // vettore che per ogni modello scalato contiene 
														 // i seguenti parametri : posizione_alfa, scala_stimata,diff_area, punteggio, current_y_scale
//#pragma omp parallel for
	for (int i = 0; i <= N; ++i)
	{

		// rapporto lunghezza larghezza nel range di valori
		const auto current_ratio = pow(max_w_h_ratio / min_w_h_ratio, double(i) / N)*min_w_h_ratio;
		// fattore di scala in Y, modifico larghezza
		const auto current_y_scale = float(model_ratio / current_ratio);
		const auto current_mesh = scaleY(main_object, current_y_scale);
		// fattore di proporzione   Lunghezza-larghezza  sul modello 3D scalato 
		const auto current_measured_ratio = distance(current_mesh.vertices(), puntatallone_index) / distance(current_mesh.vertices(), sxdx_index);
		
		
		
		// stima della rotazione del piede sul foglio-restituisce 
		//restituisce :posizione_alfa, scala_stimata
		auto ang_scale = process(contour, rettangolo, mu_orig, flagimg, current_mesh, traslazione_modello, float(pass_data.ac.fovy), float(pass_data.ac.T[0].at<double>(2, 0)));
		//ottimizza posizione sul foglio e scala opportunatamente in modo da far corrispondere larghezza proiezioen modello con sagoma foto
		/*return  tuple con :
			*					momenti dell’immagine,
			*					bool indicante se contorno trovato
			*                  area contorno
			*				   bool proiezione laterale Se true all'interno della sagoma del piede utente!
			*                  */
		auto moments = optimize_M(contour, get<0>(ang_scale), rettangolo, traslazione_modello, current_mesh, 9, 9, get<1>(ang_scale), pass_data);


		if (get<3>(moments))// proiezione laterale entro il tallone
			if (get<1>(moments)) {//contrno trovato
				//  utilizzo  dei momenti per ricavare la miglior sovrapposizione tra sagoma e proiezione modello
				auto &tmp_moments = get<0>(moments);
				auto punteggio = abs(tmp_moments.nu02 - mu_orig.nu02) + abs(tmp_moments.nu03 - mu_orig.nu03) + abs(tmp_moments.nu11 - mu_orig.nu11) + abs(tmp_moments.nu12 - mu_orig.nu12) + abs(tmp_moments.nu20 - mu_orig.nu20) + abs(tmp_moments.nu21 - mu_orig.nu21) + abs(tmp_moments.nu30 - mu_orig.nu30);
				auto t = tuple_cat(move(ang_scale), make_tuple(punteggio, current_y_scale));
				// vettore che per ogni modello scalato contiene  posizione_alfa, scala_stimata,punteggio, current_y_scale
				records.push_back(move(t));
				//	cout << i << "\t" << current_y_scale << "\t" << get<0>(t) << "\t" << get<1>(t) << "\t" << get<2>(t) << "\t" << get<3>(t) << "\t" << current_measured_ratio << "\t" << area << endl;

				//cout << " Modello " << i << "\t" << "FATTORE SCALA in y da appilcare modelo originale" << '\t' << current_y_scale << "\t" << "RAPPORTO DEL Lw MODELLO" << current_measured_ratio << endl;
				//cout << "ANGOLO " << "\t" << get<0>(t) << "\t" << " scala stima dopo process" << "\t" << get<1>(t) << endl;
				//cout << "Punteggio mu" << "\t" << get<2>(t) << "AREA" << "\t" << area << endl;
				//cout << "FATTORE SCALA FINALE DA APPICARE AL MDELLO   " << get<3>(t) << endl;
			}
			else
				cout << i << "\t" << "contorno non trovato!!!" << endl;
		else
		{	cout << i << "\t" << "piede proiettato fuori dal contorno!!!" << endl;
			//break;
		}
	}
	
	if (!records.empty())
	{// Ricerca all'interno di record dell'indice corrispondente al valore minimo di punteggio
		const auto index = min_element_fn(records.begin(), records.end(), [](auto&el) {return get<2>(el); });
		cout << "selecting index " << (index - records.begin()) << endl;
		*posizione_alfa = get<0>(*index); //angolo 
		const auto scala_stimata = get<1>(*index); //scala}
		return{ scale(scaleY(main_object, get<3>(*index)), scala_stimata), scala_stimata };
	}
	else

	{
		cout << "ERROR Nessun modello proiettato!!" << endl;// DA CORREGGERE|!!!
		return{ main_object, 1.f };
	}

	
}


// VErsione modificata .... non faccio piiu il parsing dei parametri di input ma ho solo foto alto e lag piede destro sinistro
int avvia_stima_dimensioni_piede(char* fname_above, const int flag_sx,  float (&dimensioni)[4], int *flag_risposte,  Passing_data &pass_data) {

    //Variabili modello 3D
    Mesh main_object;
    int puntatallone_index[2] = { 1381,266 }; //punta,tallone
    int sxdx_index[2] = { 1062,1616 }; // sx,dx
    //int collosucollogiu_index[2] = { 1219, 732 }; // collosu,collogiu
    float traslazione_modello;
    //Variabili configurazione spaziale
    float posizione_alfa = 0;
    //Variabili elaborazione sagoma
    Mat sagoma_img, sagoma_warp_cut;
    vector <Point> sagoma_cnt;

    //qualitaimg: numero tra 0 (qualità alta) e 209 (qualità pessima). Aumenta con blur, ombre e scarsa luminosità. -1 in caso di errore. 
    //flagsimg: 0 (stima riuscita), 1 (stima fallita a causa di un errore nelle due immagini)
    //flagimg: usa 7 bit per impostare 7 flag sull'immagine
    //    MSB    7    6    5    4    3    2    1    0    LSB
    //    bit0: se 1 ho un blur troppo alto
    //    bit1: se 1 la sagoma non è stata trovata
    //    bit2: se 1 l'inquadratura è troppo distante e la stima potrebbe risentirne
    //    bit3: se 1 il foglio non è abbastanza planare e la stima potrebbe essere inaccurata
    //    bit4: se 1 il piede è troppo in avanti e potrebbe esserci una sovrastima
    //    bit5: se 1 il piede è troppo indietro e potrebbe esserci una sottostima o un errore dovuto all'angolo
    //    bit6: se 1 la stima della scala è fallita a causa di una sagoma troppo differente dal modello

    //Parsing parametri // GIA FATTO IN BORTOLATO PART

    const string nomefile = fname_above;

	const int flag_output_3D = 1;
    try {
        const char *filename = "res/pie_inicio_midres.obj";
        //	const char *filename = "res/pie_inicio_midres_no_angled.obj";
        main_object = load_obj(filename);

        //Centratura XY del modello
        auto medio = midpoint(main_object.vertices());

        medio.z = 0; // do not center on Z

        for (auto & vertice : main_object.vertices()) {
            vertice -= medio;
        }
		traslazione_modello = 0; //(main_object.vertices()[puntatallone_index[1]].x - main_object.vertices()[puntatallone_index[0]].x) / 2.0f;
    } catch (file_not_found_error &err)
    {
        cerr << "File non trovato " << err.what() << endl;
        return 1;
    }
    if (flag_sx) {
        float miny = 2e9, maxy = 0;
        for (auto & vertice : main_object.vertices()) {
            if (vertice.y > maxy)maxy = vertice.y;
            if (vertice.y < miny)miny = vertice.y;
        }
        for (auto & vertice : main_object.vertices()) {
            vertice.y = maxy - vertice.y + miny; // PAN: Mirrors maintaining min and max y
        }
    }

    /* ----------------------------------------------------------------
     * ------------------ INIZIO ELABORAZIONE FILE --------------------
     * ----------------------------------------------------------------
     */

    int qualitaimg = 0;
    int flagimg = 0;
	const auto result = elabora_immagine(nomefile, &flagimg, &qualitaimg, IMG_WIDTH, IMG_HEIGHT, &posizione_alfa, puntatallone_index, sxdx_index, main_object, traslazione_modello, pass_data);

	if (result.second == -1)
		return -1;

	dimensioni_applicate_scala(result.first, puntatallone_index, sxdx_index, 1.0, dimensioni);
	if (flag_output_3D) {
		save_obj(result.first, (nomefile + ".obj").c_str(), 1.0);
	}

	flag_risposte[0] = qualitaimg;
	flag_risposte[1] = flagimg;
	return 0;
}

void Salva_output(const string &selector, float num)
{
	ofstream outFile;

	if (selector == "index")
	{
		outFile.open("output.txt");
	//	outFile.clear();
		outFile << "indice  " << num;
	}
	else
	{
		outFile.open("output.txt", ios::out | ios::trunc);
		//outFile.clear();
		outFile << "  lunghezza   " << num;

	}outFile.close();
	return;
}


/*
* VEcchia funzione di main Algoritmo BIocubica , ottimizzato da MArco.
   Ho tolto argv e argc e passo:
* @param nomefile Nome della foto presa da alto da elaborare.
* @param flagsx  Variabile per identificare se piede destro o sinistro.
* @param ac struttura dati che contiene matrici posa delle camere e parametri intrinseci 
* @return 0 se calcoli andati a buon fine altrmanti messaggio errore con indicazione del tipo di errore.
*/


//Process_Calibrate_Args_s ac_global;


int addiFit_biocubica(char* fname_above, const int flag_sx,  Passing_data &pass_data) {
    float dimensioni[4];
    int flag_risposte[2];
	
    const int ret = avvia_stima_dimensioni_piede(fname_above, flag_sx, dimensioni, flag_risposte,pass_data);
    if (ret != 0) {
        cout << "{ \"ok\": false, \"qualita\": " << flag_risposte[0] << ", \"flag\": " << std::bitset<8>(flag_risposte[1]) << " }" << endl;
    } else {
        cout << "{ \"ok\": true, \"punta_tallone\": " << dimensioni[3] << ", \"lunghezza\": " << dimensioni[0] << ", \"larghezza\": " << dimensioni[1] << ", \"collo\": " << dimensioni[2] << ", \"qualita\": " << flag_risposte[0] << ", \"flag\": " << std::bitset<8>(flag_risposte[1]) << " }" << endl;

	}

    return ret;
}
