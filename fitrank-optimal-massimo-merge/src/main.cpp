
#include <iostream>
#include "mb_main.h"
#include "addi-fit2_ottimizzato.h"

using namespace std;

static void print_usage() {
    const char *logo =
        "       __    ____  ____  ____    ____  ____  ____ \n"
        "      /__\\  (  _ \\(  _ \\(_  _)  ( ___)(_  _)(_  _)\n"
        "     /(__)\\  )(_) ))(_) )_)(_    )__)  _)(_   )(  \n"
        "    (__)(__)(____/(____/(____)()(__)  (____) (__)";

    cout << endl << logo << endl << endl;
    cout << "Usage: addi_fit  image_from_above_fname  image_from_ext_fname  image_from_int_fname  [debug_output_path]";
    cout << endl << endl;
}

//--------------------------------------------------------------------------------------------------
int main(int argc, char **argv) {


    // Valori di default dei parametri dell'algoritmo
    AlgoParams params{};
    params.a4_minAreaPerCent = 10;
    params.findCorners_skipPixels = 40;
    params.findCorners_roiSize = 80;
    params.findCorners_skipPixels2 = 2;
    params.ransac1_iterations = 150;
    params.ransac1_minInliersPercent = 40;
    params.ransac1_minInliersPercent_2nd = 30;
    params.ransac1_maxError = 2.0;
    params.ransac2_iterations = 100;
    params.ransac2_minInliersPercent = 70;
    params.ransac2_minInliersPercent_2nd = 35;
    params.ransac2_maxError = 2.0;

    params.heel_y_search_step = 5;
    params.heel_max_distance_between_points = 30;
    params.heel_skip_x_right = 100;
    params.heel_skip_x_left = 5;
    params.heel_derivative_step = 2;
    params.heel_derivative_apices_window = 10;
    params.heel_derivative_threshold = 10.0;
    params.heel_min_number_of_points = 40;

    params.debug = 0;
    params.debug_save_path = nullptr;


    if (argc < 4) {
        print_usage();
        return 0;
    }

    const auto fname_above = argv[1];
    const auto fname_ext = argv[2];
    const auto fname_int = argv[3];
    const int flag_sx = (std::find(argv, argv + argc, string("-sx")) != argv + argc);

    if ((std::find(argv, argv + argc, string("-save")) != argv + argc)) {
        params.debug = 3;
        if (flag_sx == 1)
        {
            params.debug_save_path = argv[6];
        }
        else
        {
            params.debug_save_path = argv[5];
        }
    }
    else {
        params.debug = 1;
        params.debug_save_path = nullptr;
    }

    Passing_data passaggio_data;
    process(fname_above, fname_ext, fname_int, &params, &passaggio_data);
    addiFit_biocubica(fname_above, flag_sx, passaggio_data);
    return 0;
}
