#pragma once

#ifndef __RENDER_H__
#define __RENDER_H__

#include <utility>

#include "mesh.h"

cv::Rect_<float> calculateBoundingBox(const Mesh &mesh3d, float traslazione_modello, float angolo, float incrementox, float incrementoy, float zmodel, float scalaattuale, float fovy, float z);
void calculatePose_M(std::vector<cv::Point>immagine, const Mesh & main_object, float traslazione_modello, float angolo, float incrementox, float incrementoy, float zfinal, float scalaattuale, float fovy, float z);
std::tuple<cv::Moments, bool, double> calculatePose(const Mesh & main_object, float traslazione_modello, float angolo, float incrementox, float incrementoy, float zfinal, float scalaattuale, float fovy, float z);
//std::tuple<cv::Moments, bool, double, float> calculatePose(const Mesh & main_object, float traslazione_modello, float angolo, float incrementox, float incrementoy, float zfinal, float scalaattuale, float fovy);
bool ProjectModel(const Mesh &mesh3d, float traslazione_modello, float angolo, float incrementox, float incrementoy, float zmodel, float scalaattuale, Passing_data &pass_data);

#endif