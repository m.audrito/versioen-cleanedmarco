#pragma once

#ifndef __UTILITY_H__
#define __UTILITY_H__

#include <vector>

#include <opencv2/core/core.hpp>

/**
 * Calcola la distanza di un punto dalla linea passante per due punti.
 * @param begin Primo punto della linea.
 * @param end Secondo punto della linea.
 * @param x Punto di cui calcolare la distanza.
 * @return Distanza tra il punto e la linea.
 */
inline double distance_to_Line(const cv::Point2f &begin, const cv::Point2f &end, const cv::Point2f &x) { // distanza di x dalla linea definita dai punti begin ed end
    const auto v = end - begin;
    return (x - begin).cross(v) / norm(v);
}

template<typename T> double sqrDistance(const cv::Point_<T> &a, const cv::Point_<T> &b) noexcept
{
    cv::Point2d v = b - a;
    return v.dot(v);
}

template<typename T> double sqrDistance(const cv::Point3_<T> &a, const cv::Point3_<T> &b) noexcept
{
    cv::Point3d v = b - a;
    return v.dot(v);
}

template<typename T> double distance(const T &a, const T &b) noexcept
{
    return sqrt(sqrDistance(a, b));
}

template<typename Container, typename T, typename EndT> double distance(const Container &container, const T &begin, const EndT &end)
{
    auto it = begin;
    double result = 0;
    while (true)
    {
        auto prev = it;
        ++it;
        if (it == end) return result;
        result += distance(container[*prev], container[*it]);
    }
}

template<typename Container, typename IndexT, int N> double distance(const Container &container, IndexT (&indices)[N])
{
    return distance(container, indices + 0, indices + N);
}

cv::Point3f midpoint(const std::vector<cv::Point3f>& vertices);

// Explicit form y = m*x + q
struct Line_s {
    double m;
    double q;
    int type;
};

Line_s fit_line_w_ransac(std::vector<cv::Point>& pp, int iterations, double maxError, int inliers_min_percent, bool first_half = false);
cv::Point2d compute_corner(Line_s& line1, Line_s& line2);
bool angle_between_lines_is_bad(const Line_s& l1, const Line_s& l2);;
std::vector<double> convolve(const std::vector<double>& a, const std::vector<double>& b);


#endif
