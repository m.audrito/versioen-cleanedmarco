#pragma once

#ifndef __DEBUGGING_H__
#define __DEBUGGING_H__

#include <vector>

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include "utility.h"

void draw_line(cv::Mat& im, Line_s& line, int len, const cv::Scalar& color);
void draw_line(cv::Mat& im, Line_s& line, const cv::Point2d& c, int len, const cv::Scalar& color);
void draw_line_strip(cv::Mat im2, const std::vector<cv::Point>& sequence);


template <typename P>
void draw_vertices(cv::Mat &im, const std::vector<P> &vv, cv::Scalar color, int thickness, bool label) {
    for (size_t i = 0; i < vv.size(); ++i) {
        auto &p = vv[i];
        circle(im, p, 7, color, thickness, cv::LINE_AA);
        if (label) {
            char txt[36];
            snprintf(txt, 36, "%i", int(i));
            putText(im, txt, p + P{ 30, 30 }, CV_FONT_HERSHEY_PLAIN, 5.0, color, 3, cv::LINE_AA);
        }
    }
}

void set_imsave_path(const char* path);
void imsave(const char* fname, const cv::Mat& im, const char* msg);
void imsave(const cv::Mat& im, const char* msg = nullptr);

#endif
