
#include <math.h>

#include "loadobj.h"
#include "addi-fit2_ottimizzato.h" // PAN: Move IMG_HEIGHT_GL and IMG_WIDTH_GL to parameters and remove this include
#include "utility.h"
#include "render.h"
#include <vector>
#include <set>
#include <map>

using namespace cv;
using namespace std;

constexpr double pi = 3.141592653589793238462643383279502884;
constexpr double piover180 = pi / 180.0;
constexpr double piover360 = pi / 360.0;

namespace {

    template<typename T>
    Point3d normalized(const Point3_<T> &p)
    {
        return Point3d(p)/norm(p);
    }

    Mat frustum(const float left, const float right, const float bottom, const float top, const float znear, const float zfar)
    {
        const auto temp = 2.0 * znear;
        const auto temp2 = double(right) - left;
        const auto temp3 = double(top) - bottom;
        const auto temp4 = double(zfar) - znear;

        return (Mat_<float>(4, 4) <<
            float(temp / temp2),
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            float(temp / temp3),
            0.0f,
            0.0f,
            float((right + left) / temp2),
            float((top + bottom) / temp3),
            float((-zfar - znear) / temp4),
            -1.0f,
            0.0f,
            0.0f,
            float((-temp * zfar) / temp4),
            0.0f);
    }

    Mat perspective(const float fovyInDegrees, const float aspectRatio, const float znear, const float zfar)
    {
        const auto ymax = float( znear * tan(fovyInDegrees * piover360));
        // ymin = -ymax;
        // xmin = -ymax * aspectRatio;
        const auto xmax = float(double(ymax) * aspectRatio);
        return frustum(-xmax, xmax, -ymax, ymax, znear, zfar);
    }

    Mat translate(Point3_<float> t)
    {
        return (Mat_<float>(4, 4) <<
            1.f,
            0.f,
            0.f,
            0.f,
            0.f,
            1.f,
            0.f,
            0.f,
            0.f,
            0.f,
            1.f,
            0.f,
            t.x,
            t.y,
            t.z,
            1.f);
    }


    Mat lookAt(const Mat& prev, const Point3f& eyePosition3D, const Point3f& center3D, const Point3f& upVector3D)
    {
        const auto forward = center3D - eyePosition3D;
        const auto forward_norm = norm(forward);
        const Point3d f{ forward.x / forward_norm, forward.y / forward_norm, forward.z / forward_norm };

        auto s = f.cross(upVector3D);
        const auto s_norm = norm(s);
        s.x /= s_norm;
        s.y /= s_norm;
        s.z /= s_norm;

        const auto u = s.cross(f);

        const auto v = prev * (Mat_<float>(4, 4) <<
            float(s.x),
            float(u.x),
            float(-f.x),
            0.0f,
            float(s.y),
            float(u.y),
            float(-f.y),
            0.0f,
            float(s.z),
            float(u.z),
            float(-f.z),
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            1.0f);
        return translate(-eyePosition3D)*v;
    }

    Mat rotate(const float angle, const Point3f &direction) {

        const auto direction_scale = norm(direction);

        const auto dx = direction.x / direction_scale;
        const auto dy = direction.y / direction_scale;
        const auto dz = direction.z / direction_scale;

        const auto a = angle * piover180; // convert to radians
        const auto s = sin(a);
        const auto c = cos(a);
        const auto t = 1.0 - c;
        const auto tx = t * dx;
        const auto ty = t * dy;
        const auto tz = t * dz;
        const auto sz = s * dz;
        const auto sy = s * dy;
        const auto sx = s * dx;

        return (Mat_<float>(4, 4) <<
            float(tx * dx + c),
            float(tx * dy + sz),
            float(tx * dz - sy),
            0.f,
            float(tx * dy - sz),
            float(ty * dy + c),
            float(ty * dz + sx),
            0.f,
            float(tx * dz + sy),
            float(ty * dz - sx),
            float(tz * dz + c),
            0.f,
            0.f,
            0.f,
            0.f,
            1.f);
    }

    Mat scale(float x, float y, float z) {
        return (Mat_<float>(4, 4) <<
            x,
            0.f,
            0.f,
            0.f,
            0.f,
            y,
            0.f,
            0.f,
            0.f,
            0.f,
            z,
            0.f,
            0.f,
            0.f,
            0.f,
            1.f);

    }
}

Mat getModelViewMatrix(const float angolo, const float traslazione_modello, const float incrementox, const float incrementoy, const float z, const float scalaattuale) {
    return
		translate({ traslazione_modello + incrementox * 0.5f*(297.0f / IMG_WIDTH_GL), incrementoy * 0.5f*(210.0f / IMG_HEIGHT_GL), z })*
		scale(scalaattuale, scalaattuale, scalaattuale)*
          rotate(5, { 1, 0, 0 })*
        rotate(-3.3f, { 0, 1, 0 })*
        rotate(+180 + angolo, { 0, 0, 1 })*rotate(180, { 1,1,0 });

}

Mat getProjectionMatrix(const float fovy, float z = 425.0f) {
    return lookAt(perspective(fovy, float(IMG_WIDTH_GL) / IMG_HEIGHT_GL, 0.1f, 10000.0f), { 0,0,z }, { 0,0,0 }, { 0,1,0 });
}

Mat getModelViewMatrix_TopView(const float angolo, const float incrementox, const float incrementoy, const float z, const float scalaattuale) {
	return
		translate({ incrementox , incrementoy , z })*
		scale(scalaattuale, scalaattuale, scalaattuale)*
		rotate(5, { 1, 0, 0 })*
		rotate(-3.3f, { 0, 1, 0 })*
		rotate(90 + angolo, { 0,0,1 })
		;


}
Point2f projectPoint(const Point3f &point, const Mat &matrix)
{
    const float *mm = reinterpret_cast<float*>(matrix.data);
    const auto w = double(mm[3]) * point.x + double(mm[7]) * point.y + double(mm[11]) * point.z + mm[15];
    const auto x = ((double(mm[0]) * point.x + double(mm[4]) * point.y + double(mm[8]) * point.z + mm[12]) / w + 1) * IMG_WIDTH_GL / 2 - CROP_GL;
    const auto y = ((double(mm[1]) * point.x + double(mm[5]) * point.y + double(mm[9]) * point.z + mm[13]) / w + 1) * IMG_HEIGHT_GL / 2 - CROP_GL;
    return { float(x), float(y) };
}

vector<Point2f> projectPoints(const vector<Point3f> &points, const Mat &matrix) {
    vector<Point2f> result;
    result.reserve(points.size());
    for (auto &point: points) {
        result.emplace_back(projectPoint(point, matrix));
    }
    return result;
}

Mat getTransformMatrix(float traslazione_modello, float angolo, float incrementox, float incrementoy, float zmodel, float scalaattuale, float fovy, float z)
{
    return getModelViewMatrix(angolo, traslazione_modello, incrementox, incrementoy, zmodel, scalaattuale) * getProjectionMatrix(fovy, z);
}

Rect_<float> calculateBoundingBox(const Mesh &mesh3d, float traslazione_modello, float angolo, float incrementox, float incrementoy, float zmodel, float scalaattuale, float fovy,  float z)
{
    const auto mat = getTransformMatrix(traslazione_modello, angolo, incrementox, incrementoy, zmodel, scalaattuale, fovy, z);

    auto minx = numeric_limits<float>::infinity();
    auto miny = numeric_limits<float>::infinity();
    auto maxx = -numeric_limits<float>::infinity();
    auto maxy = -numeric_limits<float>::infinity();

    for (auto &point : mesh3d.vertices()) {
        const auto projectedPoint = projectPoint(point, mat);

        minx = min(minx, projectedPoint.x);
        maxx = max(maxx, projectedPoint.x);
        miny = min(miny, projectedPoint.y);
        maxy = max(maxy, projectedPoint.y);
    }
    return Rect_<float>(Point2f{ float(minx), float(miny) }, Point2f{ float(maxx), float(maxy)});
}


class unordered_edge {
	int p1_, p2_;
public:

	unordered_edge(int p1, int p2) : p1_(min(p1, p2)), p2_(max(p1, p2)) {  }

	bool operator < (const unordered_edge &e) const {
		return (p1_ < e.p1_) ? true : ((p1_ == e.p1_) ? p2_ < e.p2_ : false);
	}

	int p1() const { return p1_; }
	int p2() const { return p2_; }
};
class edge{
	int p1_, p2_;
	bool inverse_;
public:

	edge(int p1, int p2) : p1_(min(p1, p2)), p2_(max(p1, p2)) { inverse_ = (p1_ != p1); }

	bool operator < (const edge &e) const {
		return (p1_ < e.p1_) ? true : ((p1_ == e.p1_) ? p2_ < e.p2_ : false);
	}

	int p1() const { return inverse_ ? p2_ : p1_; }
	int p2() const { return inverse_ ? p1_ : p2_; }
};
enum class Winding
{
	Unknown,
	Positive,
	Negative,
};

void insert_or_erase(set<edge> &edgeMap, set<unordered_edge> &tangent, Winding winding, int p1, int p2)
{
	if (winding == Winding::Unknown)
	{
		const auto it12 = edgeMap.find({ p1, p2 }); // find both 1-2 and 2-1
		if (it12 != edgeMap.end()) {
			edgeMap.erase(it12);
			return;
		}
		const auto ue12 = unordered_edge{ p1, p2 };
		const auto un12 = tangent.find(ue12);
		if (un12 != tangent.end()) {
			tangent.erase(un12);
			edgeMap.insert({ p1, p2 });
			edgeMap.insert({ p2, p1 });
			return;
		}
		tangent.insert(ue12);
		return;
	}
	edge e12{ (winding == Winding::Positive) ? p1 : p2, (winding == Winding::Positive) ? p2 : p1 };
	const auto it12 = edgeMap.find(e12);
	if (it12 != edgeMap.end()) {
		if (it12->p1() != e12.p1()) {
			edgeMap.erase(it12);
		}
		return;
	}
	const auto un12 = tangent.find({ p1, p2 });
	if (un12 != tangent.end())
	{
		tangent.erase(un12);
		edgeMap.insert({ p1, p2 });
		edgeMap.insert({ p2, p1 });
		return;
	}
	edgeMap.insert(e12);
}

set<edge> buildEdgeMap(const vector<Point2f> &projectedPoints, const vector<TriangleIndices> &elements) {
	set<edge> edgeMap;
	set<unordered_edge> tangent;

	for (const auto &el : elements) {
		const auto pp1 = projectedPoints[el.v1];
		const auto v12 = projectedPoints[el.v2] - pp1;
		const auto v13 = projectedPoints[el.v3] - pp1;

		const auto v12n = v12.x*v12.x + v12.y*v12.y;
		const auto v13n = v13.x*v13.x + v13.y*v13.y;

		const auto delta = (v12.x*v13.y - v12.y*v13.x);
		const auto epsilon = std::numeric_limits<float>::epsilon();

		const auto w = (delta*delta < epsilon*v12n*v13n) ? Winding::Unknown : (delta > 0) ? Winding::Positive : Winding::Negative;
		insert_or_erase(edgeMap, tangent, w, el.v1, el.v2);
		insert_or_erase(edgeMap, tangent, w, el.v2, el.v3);
		insert_or_erase(edgeMap, tangent, w, el.v3, el.v1);
	}

	for (const auto &ed : tangent) {
		edgeMap.insert({ ed.p1(), ed.p2() });
		edgeMap.insert({ ed.p2(), ed.p1() });
	}

	for (const auto &el : elements) {
		const auto it12 = edgeMap.find({ el.v1, el.v2 }) != edgeMap.end();
		const auto it23 = edgeMap.find({ el.v2, el.v3 }) != edgeMap.end();
		const auto it31 = edgeMap.find({ el.v3, el.v1 }) != edgeMap.end();
		if (it12  && it23 && it31) {
            const auto r =
				edgeMap.erase({ el.v1, el.v2 }) +
				edgeMap.erase({ el.v2, el.v3 }) +
				edgeMap.erase({ el.v3, el.v1 });
			assert(r == 3);
		}
	}
	return edgeMap;
}

vector<vector<int>> contourMapToLoops(multimap<int, int> &&contourMap) {
    vector<int> seq;
    vector<vector<int>> loops;
    seq.reserve(contourMap.size());

    auto it = contourMap.begin();
    int loopBegin = it->first;
    seq.push_back(loopBegin);
    while (it != contourMap.end()) {
        int target = it->second;
        contourMap.erase(it);
        it = contourMap.find(target);
        if (it == contourMap.end() && !contourMap.empty()) {
            if (target == loopBegin) {
                seq.push_back(target);
                loops.push_back(std::move(seq));
                seq.clear();
                it = contourMap.begin();
                loopBegin = it->first;
                seq.push_back(loopBegin);
            }
            else
            {
                auto it2 = contourMap.find(seq.back());
                while (it2 != contourMap.end()) {
                    seq.pop_back();
                    it2 = seq.empty() ? contourMap.end() : contourMap.find(seq.back());
                }
                if (seq.empty() && !contourMap.empty()) {
                    it = contourMap.begin();
                    loopBegin = it->first;
                    seq.push_back(loopBegin);
                }
            }
        }
        else
        {
            seq.push_back(target);
        }
    }

    if (seq.size() > 2 && seq.front() == seq.back()) {
        loops.push_back(move(seq));
    }

    return loops;
}

vector<vector<int>> findContourLoops(const vector<Point2f> &projectedPoints, const vector<TriangleIndices> &elements) {
    auto edgeMap = buildEdgeMap(projectedPoints, elements);

    multimap<int, int> contourMap;

    for (auto &em : edgeMap) {
        contourMap.insert({ em.p1(), em.p2() });
    }
    edgeMap.clear();

    return contourMapToLoops(move(contourMap));
}



vector<vector<Point>> loopsToContours(const vector<vector<int>> &loops, const vector<Point2f> &projectedPoints)
{
    vector<vector<Point>> result(loops.size());

    for (size_t i = 0; i < loops.size(); ++i) {
        vector<Point> maybeLoop;
        for (auto&el : loops[i]) {
            maybeLoop.push_back(projectedPoints[el]);
        }
        result[i] = maybeLoop; // removeLoops(move(maybeLoop));
    }
    return result;
}

vector<vector<Point>> findContours(const vector<Point2f> &projectedPoints, const vector<TriangleIndices> &elements) {
    return loopsToContours(findContourLoops(projectedPoints, elements), projectedPoints);
}



vector<Point> clip(const vector<Point> &path, float a, float b, float c) {

    // TODO: Refine clipping to cut properly at borders

    vector<Point> result;
    for (auto &p: path) {
        if (p.x*a + p.y*b + c >= 0) {
            result.push_back(p);
        }
    }
    return result;
}

// PAN: This is the only function using rendering as of today. Contour can be obtained directly by projecting the mesh and figuring out what edges compose the border (change of triangle winding in projected space)

void calculatePose_M(vector<Point>immagine, const Mesh & main_object, float traslazione_modello, float angolo, float incrementox, float incrementoy, float zfinal, float scalaattuale, float fovy, float z)
{
	const auto contours = findContours(projectPoints(main_object.vertices(), getTransformMatrix(traslazione_modello, angolo, incrementox, incrementoy, zfinal, scalaattuale, fovy, z)), main_object.elements());

	const int width = IMG_WIDTH_GL - 2 * CROP_GL;
	const int height = IMG_HEIGHT_GL - 2 * CROP_GL;


	//// TODO: Code that might be removed

	Mat imagefinale(height, width, CV_8UC1);
	memset(imagefinale.data, 0, width * height);
	fillPoly(imagefinale, contours, 115);
	Mat contorno_image(height, width, CV_8UC1);
	memset(contorno_image.data, 0, width * height);

	for (const auto& i : immagine)
	{
		// use a random color:     Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
		Scalar color = Scalar(0, 0, 240);
		circle(contorno_image, i, 2, 255, 2, 8, 0);

	}

	Mat dst;

	addWeighted(imagefinale, 1, contorno_image, 0.2, 0.0, dst);
	/*--------------------------test-----------------------------------------------*/
	Visualizza(dst, "CALCULATE POSE M POSA MODELLO e sagoma piede immagine");
	/*---------------------------------------------------------------------------*/

	vector<vector<Point> > contours2;
	vector<Vec4i> hierarchy;
	findContours(imagefinale, contours2, hierarchy, CV_RETR_LIST, CHAIN_APPROX_NONE);
	const auto c2 = max_element(begin(contours2), end(contours2), [](const auto &a, const auto &b) { return a.size() < b.size(); });
	if (c2 == end(contours2))
	{
		return;
	}
	const auto m2 = moments(*c2, true);
}

// PAN: This is the only function using rendering as of today. Contour can be obtained directly by projecting the mesh and figuring out what edges compose the border (change of triangle winding in projected space)
tuple<Moments, bool, double> calculatePose(const Mesh & main_object, float traslazione_modello, float angolo, float incrementox, float incrementoy, float zfinal, float scalaattuale, float fovy, float z)
{
	const auto contours = findContours(projectPoints(main_object.vertices(), getTransformMatrix(traslazione_modello, angolo, incrementox, incrementoy, zfinal, scalaattuale, fovy, z)), main_object.elements());

	const int width = IMG_WIDTH_GL - 2 * CROP_GL;
	const int height = IMG_HEIGHT_GL - 2 * CROP_GL;



	//// TODO: Code that might be removed
	Mat imagefinale(height, width, CV_8UC1);
	memset(imagefinale.data, 0, width * height);
	fillPoly(imagefinale, contours, 100);

	vector<vector<Point> > contours2;
	vector<Vec4i> hierarchy;
	findContours(imagefinale, contours2, hierarchy, CV_RETR_LIST, CHAIN_APPROX_NONE);
	const auto c2 = max_element(begin(contours2), end(contours2), [](const auto &a, const auto &b) { return a.size() < b.size(); });
	if (c2 == end(contours2))  //DA VDER BENE!!!
	{
		return{ {}, false, 0 };
	}
	const auto m2 = moments(*c2, true);
	return{ m2, true, contourArea(*c2) };
}

vector<Point2f> Get_Rect_unwarping(const Mesh &mesh3d, const Mat &img_top, const Mat &inv_warp_mat, const Mat &biocubica_trasf_mat)
{
	Mat  overlay;
	img_top.copyTo(overlay);// copy the source image to an overlay

	vector<Point2f> tp_ver;
	vector<Point2f> vc_center, cc;
	vc_center.emplace_back(0.f,0.f);
	vc_center.emplace_back((IMG_WIDTH_GL - 2 * CROP_GL - 1) / 2.f, (IMG_HEIGHT_GL - 2 * CROP_GL - 1) / 2.f );
	vc_center.emplace_back(float(IMG_WIDTH_GL), 0.f);

	/*-----------metodo biocubica-----*/
	// calcolo posizione del centro del sistema riferimento del piano immagine 
	perspectiveTransform(vc_center, cc, inv_warp_mat);

	// proiezione di tutto il modello sul piano immagine top view  
	vector<Point2f> vertici = projectPoints(mesh3d.vertices(), biocubica_trasf_mat);
	for (auto &p : vertici) {
		p.x += CROP_GL;
		p.y += CROP_GL;
	}
	perspectiveTransform(vertici, tp_ver, inv_warp_mat);

	return(tp_ver);


}



void Visualizza_punti(Mat img, vector<Point3f> contour_points, const string s)
{


	Visualizza(img, s);

}

void Visualizza_punti(Mat img, vector<Point2f> contour_points, const string s)
{

	for (auto &p : contour_points)
	{
		circle(img, Point(lround(p.x), lround(p.y)), 12, CV_RGB(255, 0, 0), -3);
		cout << "contorno  " << p << endl;
	}
	Visualizza(img, s);

}

// Funzione che trva la posizione del modello ful foglioA$
Mat optimize_Model_on_View(const Mesh &mesh3d, const Mat &img_top, Rect rettangolo_sagoma, float angolo, float &incrementox, float &incrementoy, float scala, Mat R0, Mat T0, const Passing_data &pass_data) {

	Mat overlay3, overlay2, overlay;

	float incremento_y = 0;// -incrementox * (297.0f / IMG_WIDTH_GL);
	float incremento_x = 0; //(incrementoy)*(210.0f / IMG_HEIGHT_GL);
	float scalaattuale = scala;
	Mesh t_Mesh;
	Mat M;
	vector<Point2f> pvertices;
	int temp_i = -1;
	for (int i = 0; i < 50; i++) {
		img_top.copyTo(overlay2);
		temp_i++;

		// Mat_RT
		M = getModelViewMatrix_TopView(angolo, incremento_x, incremento_y, 0, scalaattuale);
		t_Mesh = Scale_translate_Model(mesh3d, M);

		projectPoints(t_Mesh.vertices(), R0, T0, pass_data.ac.K, pass_data.ac.D, pvertices);

		const Rect_<float> rettangolo3 = boundingRect(pvertices);//
																 // PAN: Scale and crop as needed. Use proper bbox intersection rather than this hand-baked version.
																 /*const Rect rettangolo3(
																 Point2i{ std::max(int(r2.tl().x), 0), std::max(int(r2.tl().y), 0) },
																 Point2i{ std::min(int(r2.br().x), IMG_WIDTH_GL - 2 * CROP_GL - 1),  std::min(int(r2.br().y), IMG_HEIGHT_GL - 2 * CROP_GL - 1) }
																 );*/
		const auto contours2 = findContours(pvertices, t_Mesh.elements());
		fillPoly(overlay2, contours2, 215);// draw a filled, contourson the overlay copy
		addWeighted(overlay2, 0.3, img_top, 0.7, 0, overlay2);// blend the overlay with the source image
		rectangle(overlay2, rettangolo_sagoma.tl(), rettangolo_sagoma.br(), CV_RGB(250, 0, 0), 2);
		rectangle(overlay2, rettangolo3.tl(), rettangolo3.br(), CV_RGB(250, 0, 0), 2);
		const auto deltax = -(rettangolo3.br().x - rettangolo_sagoma.br().x);
		const auto deltay = ((rettangolo3.y * 2 + rettangolo3.height) - (rettangolo_sagoma.y * 2 + rettangolo_sagoma.height));
		const auto deltascala = rettangolo3.height - rettangolo_sagoma.height;
		//	cout << "conteggio passi ottimizzazine Modello su Massi TOP  VIEW    " << i << endl;
			/*--------------------------test-----------------------------------------------*/
		Visualizza(overlay2, "PROIEZONE sagoma top VIEW");
		/*---------------------------------------------------------------------------*/

		if (deltax == 0 && deltay == 0 && (((deltascala == 0))))
		{
			//	cout << "\n uscita con delta x:" << deltax << "\t uscita con delta y:" << deltay << "\t uscita con deltascala:" << deltascala <<"\n";
			break;
		}
		incremento_x += deltax * (210.0f / (pass_data.a4_1.vertices[1].x - pass_data.a4_1.vertices[0].x)) / 2.0f; // / 2.0 is an arbitrary scale factor - this could be adjusted using the modelview/projection matrix
		incremento_y += deltay * (290.0f / (pass_data.a4_1.vertices[2].y - pass_data.a4_1.vertices[0].y)) / 4.0f;
		scalaattuale = scalaattuale * rettangolo_sagoma.height / rettangolo3.height;
	}
	const auto contours2 = findContours(pvertices, t_Mesh.elements());
	fillPoly(overlay2, contours2, 215);// draw a filled, contourson the overlay copy
	addWeighted(overlay2, 0.3, img_top, 0.7, 0, overlay2);// blend the overlay with the source image
	rectangle(overlay2, rettangolo_sagoma.tl(), rettangolo_sagoma.br(), CV_RGB(250, 0, 0), 2);
	/*--------------------------test-----------------------------------------------*/
	//Visualizza(overlay2, "PROIEZONE FINALE sagoma top VIEW");
	/*---------------------------------------------------------------------------*/

	incrementox = incremento_x;
	incrementoy = incremento_y;
    return M;
}


/* Fproietta il modello sulle foto laterali mediante le matrici di posa ricavate*/
bool ProjectModel_Lat(const Mesh &mesh3d, const Mat R, const Mat T, const Mat K, const Mat D, Mat &img_Lat, const vector<Point> profile)
{
	vector<Point2f> pvertices_lat, tp_ver_lat;
	Mat overlay;
	img_Lat.copyTo(overlay);


	/*Proiezione modello su immagine laterale*/
	/*---------------test------------------------------------- */
	projectPoints(mesh3d.vertices(), R, T, K, D, pvertices_lat);
	const auto contours = findContours(pvertices_lat, mesh3d.elements());
	fillPoly(overlay, contours, CV_RGB(255, 0, 0));// draw a filled, contours on the overlay copy
	addWeighted(overlay, 0.3, img_Lat, 0.7, 0, overlay);
    if (contours.empty())
        return false;
    int max_size_contour = contours[0].size();
    int j = 0, i = 0;
    for (auto& p : contours)
    {
        if (p.size() > max_size_contour)
        {
            max_size_contour = p.size();
            j = i;
        }

        i++;
    }
    Rect box__Lat_Foot = boundingRect(contours[j]);

    rectangle(overlay, box__Lat_Foot.tl(), box__Lat_Foot.br(), CV_RGB(0, 250, 0), 2);

    /*-------------------------------------------------------- */
	/*Proiezione su immagine laterale del tallone*/
	vector<Point3f> vpoint;
	vector<Point2f> vpoint_prj;
	vpoint.emplace_back(mesh3d.vertices()[HEEL_VERTEX].x,mesh3d.vertices()[HEEL_VERTEX].y,mesh3d.vertices()[HEEL_VERTEX].z);
	vpoint.emplace_back(mesh3d.vertices()[TOE_VERTEX].x,mesh3d.vertices()[TOE_VERTEX].y,mesh3d.vertices()[TOE_VERTEX].z);
	projectPoints(vpoint, R, T, K, D, vpoint_prj);

	const Point Heel_pt_on_model = vpoint_prj[0];
	Point Heel_pt_on_profile;

	/*Ricerca del punto sull'immagine corrispondente al profilo del tallone data la Y del punto sul tallone del modello 3D*/
	/* TO DO: trovare un mode piu intelligente rispetto alla ricerca sul profilo del punti con Y +/6 pixels dalla Y punto del Modello 3d del tallone*/
	circle(overlay, Point2f{ vpoint_prj[0].x,vpoint_prj[0].y }, 12, CV_RGB(0, 255, 0), -3);
	for (auto &p : profile)
	{
		if ((Heel_pt_on_model.y - 3 <= p.y) && (p.y <= Heel_pt_on_model.y + 3))
		{
			Heel_pt_on_profile = p;
			/*cout << "Pt on profile   " << Heel_pt_on_profile << endl;
			cout << "tallone		" << Heel_pt_on_model << endl;*/
			circle(overlay, p, 12, CV_RGB(0, 0, 250), -3);
			//	break;
		}

	}

    const bool check_position = Heel_pt_on_profile.x < Heel_pt_on_model.x;

    /*--------------------------test-----------------------------------------------*/
    Visualizza(overlay, "PROIEZONE sagoma piede immagine");
    /*---------------------------------------------------------------------------*/
    return check_position;
}

/* Funzione che trova la posizione del modello sul foglio A4 e poi proietta il modello sulle foto laterali mediante le matrici di posa ricavate*/
bool ProjectModel(const Mesh &mesh3d, float traslazione_modello, float angolo, float incrementox, float incrementoy, float z, float scalaattuale,  Passing_data &pass_data)
{

    /* Prova Proiezione vista top*/

    Mat img_top;
    pass_data.a4_1.im.copyTo(img_top);

    Mat R0, T0; //Matrici rot e trasla per lat ext
    //Rodrigues(pass_data.ac.R[0], R0);
    pass_data.ac.R[0].copyTo(R0);
    pass_data.ac.T[0].copyTo(T0);


    Mat Warp_inv, tmp, tmp_proj_biocubica;
    invert(pass_data.warping, Warp_inv);
    Mat inv_warp_mat;
    Warp_inv.convertTo(inv_warp_mat, CV_64F);


    Mat img2, img3, img4;
    pass_data.a4_1.im.copyTo(img2);
    pass_data.a4_1.im.copyTo(img3);

    // Translate and scale the model 

    const auto biocubica_trasf_mat = getTransformMatrix(traslazione_modello, angolo, incrementox, incrementoy, z, scalaattuale, float(pass_data.ac.fovy), float(pass_data.ac.T[0].at<double>(2, 0)));
	
    // ricavo il rettangolo del mdello di piede 3d proiettato sullo schermo e mediante unwarping lo trovo sull'immagine TOP di input
    vector<Point2f> pvertices2;
    const vector<Point2f> tp_ver = Get_Rect_unwarping(mesh3d, img3, inv_warp_mat, biocubica_trasf_mat);

    const Rect rettangolo_contorno_trasformato = boundingRect(tp_ver);

    // il rettangolo trovato � il riferimento per posizionare il modello 3d sul foglio A4 sull'immagine di inut

    const Mat RT_Model = optimize_Model_on_View(mesh3d, img3, rettangolo_contorno_trasformato, angolo, incrementox, incrementoy, scalaattuale, R0, T0, pass_data);

    const Mesh Mesh_Model_on_A4_Sheet = Scale_translate_Model(mesh3d, RT_Model);



    /* -----------------------------------------------------------------------------------*/
    /* applico proiezione  del modello vista laterale ext*/
    Mat R1, T1; //Matrici rot e trasla per lat ext
    //Rodrigues(pass_data.ac.R[1], R1);
    pass_data.ac.T[1].copyTo(T1);
    pass_data.ac.R[1].copyTo(R1);
    Mat img_Lat_ex;
    pass_data.a4_2.im.copyTo(img_Lat_ex);
    const bool check = ProjectModel_Lat(Mesh_Model_on_A4_Sheet, R1, T1, pass_data.ac.K, pass_data.ac.D, img_Lat_ex, pass_data.im2_heel_c_profile);

    /* -----------------------------------------------------------------------------------*/
    /* applico proiezione  del modello vista laterale int*/
    Mat R2, T2; //Matrici rot e trasla per lat ext
    //Rodrigues(pass_data.ac.R[2], R2);
    pass_data.ac.T[2].copyTo(T2);
    pass_data.ac.R[2].copyTo(R2);
    Mat img_Lat_int;
    pass_data.a4_3.im.copyTo(img_Lat_int);
    const bool check2 = ProjectModel_Lat(Mesh_Model_on_A4_Sheet, R2, T2, pass_data.ac.K, pass_data.ac.D, img_Lat_int, pass_data.im3_heel_c_profile);

    return (check && check2);
}



