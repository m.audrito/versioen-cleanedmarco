
#pragma once

#include <opencv2/opencv.hpp>
#include <vector>
#include <string>
#include "mesh.h"


#include "mb_main.h"



//Dimensioni immagine
#define IMG_WIDTH 1200
#define IMG_HEIGHT 900

//Dimensioni immagine virtuale
#define IMG_WIDTH_GL 1272
#define IMG_HEIGHT_GL 900
#define CROP_GL 50

//Costanti limite correzione contrasto
#define MIN_M 105
#define MAX_M 135
#define MIN_E 103
#define MAX_E 110

// Veritici modello mesh
#define HEEL_VERTEX 266
#define TOE_VERTEX 1381
//int puntatallone_index[2] = { 1381,266 }; //punta,tallone
//int sxdx_index[2] = { 1062,1616 }; // sx,dx

//Funzioni di stima

/**
 * Stima la rotazione del piede sul foglio individuando l’angolo associato ad una massima similarità tra la sagoma elaborata e la sagoma virtuale.
 * @param rettangolo Bounding box del contorno del piede trasformato.
 * @param mu_orig momenti originali del contorno del piede.
 * @param traslazione_modello Traslazione del modello 3D sul piano del foglio.
 * @param main_object Modello 3D del piede virtuale.
 * @return Variabile per il salvataggio dell’angolo stimato.
 */
float correggirotazione(const cv::Rect &rettangolo, const cv::Moments &mu_orig, float traslazione_modello, const Mesh &main_object, float fovy);

/**
 * Calcola il fattore di scala del modello in grado di massimizzare il matching con la sagoma trasformata.
 * @param rettangolo Bounding box del contorno del piede trasformato.
 * @param mu_orig momenti originali del contorno del piede.
 * @param dimensionemin Dimensione punta-tallone minima da ricercare.
 * @param dimensionemax Dimensione punta-tallone massima da ricercare.
 * @param flagimg Variabile per il salvataggio dei flag descrittivi dell’immagine.
 * @param puntatallone_index Vettore contenente l’indice dei punti di interesse del modello (punta, tallone).
 * @param main_object Modello 3D del piede virtuale.
 * @param traslazione_modello Traslazione del modello 3D sul piano del foglio.
 * @param sagoma_warp_cut Immagine della sagoma trasformata.
 * @param posizione_alfa Angolo di rotazione del piede stimato.
 * @param scala_stimata Variabile per il salvataggio della scala stimata.
 * @return Codice di uscita 3 in caso di fallimento e 0 in caso di successo.
 */
float ottimizzascala2_fast(const cv::Rect &rettangolo, const Mesh &main_object, float traslazione_modello, float posizione_alfa, float fovy, float z);

/**
 * Salva un modello 3D del piede virtuale avente le dimensioni stimate in formato OBJ.
 * @param mesh Modello 3D del piede virtuale.
 * @param nomefile Nome del file OBJ che conterrà il modello 3D.
 * @param scala Fattore di scala da applicare al modello.
 */
void salva_output_3D(const Mesh &mesh, const std::string &nomefile, float scala);

/**
 * Applica il fattore di scala fornito al modello e calcola lunghezza, larghezza, collo e distanza punta-tallone del piede stimato.
 * @param modello_piede Modello 3D del piede virtuale.
 * @param puntatallone_index Vettore contenente l’indice dei punti di interesse del modello (punta, tallone)
 * @param sxdx_index Vettore contenente l’indice dei punti di interesse del modello (estremo sinistro, estremo destro).
 * @param scala Fattore di scala da applicare al modello.
 * @param dimensioni Vettore per il salvataggio delle dimensioni calcolate.
 */
void dimensioni_applicate_scala(const Mesh &modello_piede, const int(&puntatallone_index)[2], const int(&sxdx_index)[2], float scala, float (&dimensioni)[4]);

/**
 * Avvia l’esecuzione dell’algoritmo con le opzioni selezionate e calcola il risultato partendo dalle elaborazioni delle due immagini.
 * @param fname_above File foto alto.
 * @param flag_sx flag che indica piede sinistro.
 * @param dimensioni Vettore per il salvataggio delle dimensioni calcolate.
 * @param flag_risposte Vettore per il salvataggio dei flag di descrizione delle immagini.
 * @param ac struttura dati che contiene matrici di calibrazione e posa delle camere
 * @return Codice di errore -1 in caso di fallimento o 0 in caso di successo.
 */
int avvia_stima_dimensioni_piede(char* fname_above, const int flag_sx, float(&dimensioni)[4], int *flag_risposte,  Passing_data &pass_data);

int addiFit_biocubica(char* fname_above, const int flag_sx,  Passing_data &pass_data);

Mesh scale(const Mesh& mesh, const float current_scale);
Mesh Scale_translate_Model(const Mesh& mesh, cv::Mat mat);
cv::Point3f MoveModelPoint3D(const cv::Point3f &point, const cv::Mat &matrix);

/**
* VIsualizza su schermo l'immagine .
* @param img immagine.
* @param str intestazione immagine

*/
void Visualizza(const cv::Mat img, std::string str);