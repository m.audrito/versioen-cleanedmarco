
#include "loadobj.h"
#include "utility.h"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace cv;
using namespace std;

Mesh load_obj(const char* filename) { // carica un file obj nel formato Wavefront
    vector<cv::Point3f> vertices;
    vector<TriangleIndices> elements;
    ifstream in(filename, ios::in);
    if (!in) { throw file_not_found_error(filename); }
    int contavertici = 0, contafacce = 0;
    string line;
    while (getline(in, line)) {
        if (line.substr(0, 2) == "v ") {
            contavertici++;
            istringstream s(line.substr(2));
            cv::Point3f v;
            s >> v.x; 
            s >> v.y; 
            s >> v.z;
            vertices.push_back(v);
        }
        else if (line.substr(0, 2) == "f ") {
            contafacce++;
            std::istringstream s(line.substr(2));
            short a, b, c;
            s >> a; 
            s >> b; 
            s >> c;
            a--; 
            b--; 
            c--;
            elements.push_back({ a, b, c });
        }
    }
    return { vertices, elements };
}

void save_obj(const Mesh &mesh, const char *nomefile, float scala) {
    ofstream out(nomefile, ios::out);
    const auto medio = midpoint(mesh.vertices());
    for (auto & vertice : mesh.vertices()) {
        const auto v = (vertice - medio)*scala + medio;
        out << "v " << v.x << " " << v.y << " " << v.z << endl;
    }
    out << endl;
    for (auto & element: mesh.elements()) {
        out << "f " << (element.v1 + 1) << " " << (element.v2 + 1) << " " << (element.v3 + 1) << endl;
    }
    out.close();
}
