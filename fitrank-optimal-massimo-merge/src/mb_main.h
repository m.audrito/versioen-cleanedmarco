#pragma once

#include <opencv2/core/core.hpp>

// VErsione creata per permettere il passaggio di parametri alla seconda parte della soluzione, versione di biocubuca
//--------------------------------------------------------------------------------------------------
/// Struttura dei parametri dell'algoritmo
///
struct AlgoParams {
	int a4_minAreaPerCent;

	int findCorners_skipPixels;
	int findCorners_roiSize;
	int findCorners_skipPixels2;

	int ransac1_iterations;
	int ransac1_minInliersPercent;
	int ransac1_minInliersPercent_2nd;
	double ransac1_maxError;

	int ransac2_iterations;
	int ransac2_minInliersPercent;
	int ransac2_minInliersPercent_2nd;
	double ransac2_maxError;

	int heel_y_search_step;
	int heel_max_distance_between_points;
	int heel_skip_x_right;
	int heel_skip_x_left;
	int heel_derivative_step;
	int heel_derivative_apices_window;
	double heel_derivative_threshold;
	int heel_min_number_of_points;

	int debug;
	const char *debug_save_path;
};


//--------------------------------------------------------------------------------------------------
struct Mask_s {
	cv::Mat mask;
	cv::Rect bb;
};


//--------------------------------------------------------------------------------------------------
struct Process_A4_s {
	cv::Mat im;
	std::vector<cv::Point2f> vertices;
	Mask_s a4_mask;
};


//--------------------------------------------------------------------------------------------------
struct Profile_s {
	std::vector<cv::Point> profile;
	std::vector<cv::Point> heel_pts;
};


//--------------------------------------------------------------------------------------------------
struct Process_Calibrate_Args_s {
	cv::Size imsz;
	std::vector<cv::Point2f> pts1;
	std::vector<cv::Point2f> pts2;
	std::vector<cv::Point2f> pts3;
	cv::Mat K;      // camerra matrix
	cv::Mat D;      // distortion coefficients
	std::vector<cv::Mat> R;  // rotation vectors
	std::vector<cv::Mat> T;  // translation vectors
	cv::Mat H2; // homography matrices
	cv::Mat H3;
	double 	fovx;// field of view in degrees along the horizontal sensor axis.
	double 	fovy;// field of view in degrees along the vertical sensor axis.

};


//--------------------------MASSI--------------------------///
// struttura di pasaggio dati per biocubica
struct Passing_data {
		
	    Process_Calibrate_Args_s ac;
		Process_A4_s a4_1;
		Process_A4_s a4_2;
		Process_A4_s a4_3;
		std::vector<cv::Point> im2_heel_profile;
		std::vector<cv::Point> im2_heel_c_profile;
		std::vector<cv::Point> im2_heel_pts;
		std::vector<cv::Point> im3_heel_profile;
		std::vector<cv::Point> im3_heel_c_profile;
		std::vector<cv::Point> im3_heel_pts;
		cv::Mat warping;  // utilizzata per capire in biocubica il passaggio da foto alto a mappatura 
		
		//std::vector<cv::Point3f> contour_top_img;
};

int process(const char *image_above_fname, const char *image_external_fname, const char *image_internal_fname, const AlgoParams *params, Passing_data *passaggio_data);